title: Oracle for Consentful Publishing Infrastructures
type: workshop
summary: This workshop presents a hands-on card desk that allows to collectively envision and share ideas for consentful publishing infrastructures. What kind of present with publishing can we imagine? How can less extraction be woven into an industry with an enormous ecological impact? What kinds of questions do we need to be asking? How do you think consent with publishing all the way through? This oracle is inspired by the [Oracle for Transfeminist Technologies](https://www.transfeministech.codingrights.org/), created by Coding Rights & MIT co-design studio.
trees: Sonian Forest
humans: Loren Britton
    students ESA St-Luc
language: English
published: November 2021
license: <a href="https://gitlab.constantvzw.org/unbound/cc4r">Collective Conditions for (re-)use (CC4r), June 2021</a>
image: algoliterary-works/2-Value-Cacao.jpg
support: This workshop was a creation for the students of ESA St-Luc in Brussels, with the support of <a href="https://www.frs-fnrs.be/fr/financements/credits-et-projets/frart">FRArt</a>


Welcome to the workshop engaging the **Oracle for Consentful Publishing Infrastructures** that we are currently developing.
To begin read aloud these **collective conditions** from below. Everyone in the space (on or offline) is invited to read one condition outloud, and the next person can read the next one.
The purpose of these **collective conditions** is to create intentional ways of being together in any space that create collective access for many bodyminds.

# C o l l e c t i v e  C o n d i t i o n s #
- If anyone would like to share the duties of translating, let's talk about how this can happen.
- No "well actually's" --> don't correct others on minor mistakes ‘well ACTUALLY a TLS handshake...’, <https://www.recurse.com/>.
- One diva one mic --> don't interrupt.
- If you would like anyone to slow down, please ask or raise your hand.
- Privacy --> personal stories stay within the room.
- Movement is good and taking care of your own needs is good leaving space for silence is good.
- Leaving the room and coming back in is good.
- Be mindful of speaking times and use your voice to speak up.
- This is a space that welcomes all genders, is decidedly anti-racist and anti- ableist, there is no room for -isms such as: classism, racism, ableism, anti- queer sentiment, misogyny, ageism, religious discrimination or any other form of discrimination and oppression.
- Not all disabilities are apparent, please don't assume what is normal for other people.
- Not all genders are apparent, please don't assume someone else's gender based on their presentation.
- There is as many teachers in this room as there are people in this space.
- This is a multi lingual space. Please ask for clarification or translation.
- Caretaking is a responsibility shared by everyone --> please feel free to check in with your neighbor, and neighbors feel free to say yes or no.
- When mistakes happen: Let us make sure that people affected by a mistake receive the care they need. Let us set further conditions to make sure that mistakes are not repeated and that we as a group can account for them.
- If you bring material into the space that might have an emotional impact on the people in the space, please share a content warning.

After reading these out loud together, please discuss them amongst the group. Are all of the words clear to everyone? Does anyone disagree with any of these conditions? Would anyone prefer a reformulation of how they are articulated?

Please begin by reading the text: *A Few Rules for Predicting the Future* by Octavia Butler, published in Essence Magazine in 2000. A link to it is here: <http://kalamu.com/neogriot/2013/07/09/history-octavia-butler-gave-us-a-few-rules-for-predicting-the-future/>

After reading, please discuss with your group what came up. What were you surprised by? What not? What do you think it means to predict the future as Octavia Butler suggests it?
While you are discussing assign one person to make three lists of the: values, objects and situations that emerge from reading Butler's text?

After some discussion, please *take a break of at least 15 min*.

Welcome back to the **Oracle for Consentful Publishing Infrastructures**.
How to use the *Oracle for Consentful Publishing Infrastructures*
Welcome! This oracle is a hands-on card deck designed to help us collectively envision and share ideas for what consentful publishing infrastructures might be... or might not be.
The game is part of Anaïs Berck's residency exploring the concept of an Algoliterary Publishing House. As Anaïs Berck represents a collective of humans, trees and algorithms, the algoliterary publishing house will function within this collective.
This means that we will have to rethink all aspects of publishing in collaboration with all entities involved. This raises a series of questions, like: how can questions around ethics and ecology be thought alongside print or online publishing? What does consent mean when working with trees and more than humans?
To begin this workshop, please start with a definition of consent, publishing and infrastructure as these are the terms that you are asked to work with in this workshop.

The framework of **consent** that we are thinking with is based on the principles of FRIES, which is easy to remember because FRIES are delicious. :) So for us we think about consent that is: Freely Given, Reversable, Informed, Enthuastic and Specific. This means that it is specific to each circumstance, aware of all of the information needed to make a decision, can be stopped at any time: not coercive -- it is not something that is wishy-washy or un-enthuastic and it is understandable as a clear 'yes' from both parties.
In this research we are working with, we think about publishing as a yet indeterminate practice - which is why we are thinking with this oracle. Publishing could mean - to publish a book, but it also could mean to think about growth as publishing or reciept printing as publishing. We are thinking about taking publishing to be a process that is about dissemination / sharing - but leave open ended the methods of how this sharing unfolds.
In this research we think with infrastructures that are temporary and specific. Infrastructure could mean a building or bureaucratic system but it also could be the format of a book. Infrastructures are repeatable, require care and can make sharing possible when it otherwise might not be.
The wisdom of the oracle is questionable, sometimes embedded with values we wish to pursue, sometimes not. We offer this as an unstable tool to consider what values are often already embedded in technological infrastructures and which ones are not. What kind of inventing, play, agencies and collaborations will be necessary to remake the world of publishing with care for more-than-humans included? How does consent work with more-than-humans including trees, algorithms, machines and more?
This oracle has been inspired and iterated on by the *Oracle for Transfeminist Tech*, which we love and can be found and learned about here: <https://www.transfeministech.codingrights.org/>.

Goal: The purpose of the **Oracle for Consentful Publishing Infrastructures** is to create ideas for technologies that are imbued with values that will remake the world so that many worlds can fit.

Take anywhere between 10 / 20 minutes for each step depending on how long you have for the activity.
11:15 - 12:30

**1. Consult the Oracle**
Each group/person takes:
1 or 2 Values Cards
1 Object Card
1 People and Places Card 1 Situation Card

**2. Read your cards**
_Value card(s): Each Value card represents a value from the texts we have shared in the reader but also that take into account the lectures from Monday. Take a moment to reflect individually and/or discuss in your group, what this value means to you. Do you wish to take this value into your work or leave it behind?
If you decide to leave it behind, keep working with this value and write down three steps in each direction away from this value, what is the most intense version of this value, what is the furthest value from this one? For example if you had 'optimisation' - perhaps the steps towards most intensity of this value would be: extraction, removal of bodies, automation - and the steps away from optimisation might be - slowing down, working with problems, doing very little. Consider what this spectrum of values around 'optimisation' is and keep brainstorming until you have an intensity of this value that works for you. Once you have finalised this, continue on.
_Object card: Each Object card represents an object referenced in our text from the reader or from the lectures on Monday.
_People/Places card: in the people/places cards all say the same thing. "Be in your own body. Be in the place you inhabit." Some speculative design exercises encourage you to imagine that you are another kind of person, but trans*feminism encourages us to recognize the importance of situated and embodied knowledge. Instead of pretending that you are someone else and you know what they need, take a moment to reflect on who you are. What body are you in? Where do you live? What privileges and burdens do your body and place provide?
_Situation card: Situations cards provide you with a situation that you need to deal with. This is a way to focus your energy on an apparatus that you might want to transform.

**3. Create your Blueprint**
Your goal is to envision a consentful publishing infrastructure/technology that:
_is embodied in your Object;
_is guided by your Values;
_is developed by and for your People, in your Place;
_and will help you work with the situation presented to you by the oracle.
Remember, it's the future, so objects are not limited by today's capabilities... everything's possible!
This speculative exercise should have a title, a diagram or drawing of your technology, and a brief description of how it works and what it creates.

**4. Share your ideas**
At the end: Share your ideas back with the others! Share your ideas about how this process did or did not work for you with us! As this is still in progress and we are accumulating ideas/processes & methods. Very happy to hear your thoughts on this work.

Thank you for participating in this workshop. These instructions are available to be iterated on or borrowed to lead your own workshop. Please do let us know at publishinghouse@anaisberck.be if you would like to be in dialogue!
