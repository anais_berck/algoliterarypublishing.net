title: Tree(s) sort(ed)
type: poster
summary: The generator of this poster is the tree sort algorithm, the subject are all the trees, algorithmic exercises and human beings involved in the <a href="https://algoliterarypublishing.net/activities.html">residency of Anaïs Berck at the Villa Empain and ESA Saint-Luc Bruxelles</a> in October 2021. The images and texts are sorted following different criteria: time, colour and the natural element in the alt-text description of the images.
algorithm: Tree sort
trees: beeches, oaks, maples, chestnuts in Bois de la Cambre/Ter Kameren Bos and Sonian Forest
humans: 60 students of ESA Saint-Luc Bruxelles, Jara Rocha, Outi Laiti, Stephan Kampelmann, Nathalie Grandjean, Loren Ren Britton, Guillaume Slizewicz, Gijs de Heij, An Mertens.
language: English
published: 2022
license: [Collective Conditions for (re-)use (CC4r), June 2021](https://gitlab.constantvzw.org/unbound/cc4r)
repository: https://gitlab.constantvzw.org/anais_berck/trees-sorted
support: These posters are a creation for the exhibition in the framework of the Open Door Days in [ESA Saint-Luc Bruxelles](http://www.stluc-bruxelles-esa.be/) with the support of [FRArt](https://www.frs-fnrs.be/fr/financements/credits-et-projets/frart).
repository: https://gitlab.constantvzw.org/anais_berck/trees-sorted
thanks: Danielle Lenaerts, Mark Streker
image: algoliterary-works/description_images_random-resampled.png
links: [online publication](https://frart.algoliterarypublishing.net/treesort/)
    [sorted by date](https://cloud.constantvzw.org/s/Wt5nBpyH6LLqWLa)
    [sorted by colour](https://cloud.constantvzw.org/s/pxiJkrBAjGn7DTx)
    [sorted by natural element](https://cloud.constantvzw.org/s/33XJw84Qn3t5zY8)
    [sorted by image description](https://cloud.constantvzw.org/s/JndqK7J78HCxKnX)

The generator of this poster is the tree sort algorithm, the subject are all the trees, algorithmic exercises and human beings involved in the <a href="https://algoliterarypublishing.net/activities.html">residency of Anaïs Berck at the Villa Empain and ESA Saint-Luc Bruxelles</a> in October 2021. The images and texts are sorted following different criteria: time, colour and the natural element in the alt-text description of the images.
<a href="https://en.wikipedia.org/wiki/Tree_sort">Tree sort</a> or binary search tree allows for fast lookup, addition, and removal of data items, and can be used to implement dynamic sets and lookup tables. The basic operations include: search, traversal, insert and delete.They are used in relational databases, data compression code, in Unix kernels for managing a set of virtual memory areas, and more.  The binary search tree algorithm was discovered independently by several researchers, including P.F. Windley, Andrew Donald Booth (UK), Andrew Colin (UK), Thomas N. Hibbard (US), and attributed to Conway Berners-Lee (UK) and David Wheeler (UK), in 1960 for storing labelled data in magnetic tapes.
