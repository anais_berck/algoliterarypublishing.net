title: Rewilding Specimens
type: installation
summary: In this installation you encounter a few taxonomists who renamed plants collected between 1885 and 1960 in Belgian Congo. For this occasion the virtual spirits of the type specimens who once lived a flourishing life in Congo, Rwanda and Burundi, left the herbarium to rename the taxonomists and rewild their portraits.
algorithm: Open CV contour & collage, selenium scraping
trees: all type specimens in [Botanical Collections Belgium](https://www.botanicalcollections.be), collected between 1885 and 1960 in Congo, Rwanda and Burundi
humans: Brendan Howell, An Mertens
language: English, French, Dutch
on_show: Constant_V, June-Sept 2022
image: algoliterary-works/John_Lindley.png
pictures: <a href = "https://piwigo.constantvzw.org/index.php?/category/4893">https://piwigo.constantvzw.org/index.php?/category/4893</a>
license: <a href="https://gitlab.constantvzw.org/unbound/cc4r">Collective Conditions for (re-)use (CC4r), June 2021</a>
repository: https://gitlab.constantvzw.org/anais_berck/rewilding-specimens
support: This experiment was a creation for the exhibition in the framework of [Constant_V](https://constantvzw.org/site/Constant_V-An-Algoliterary-Publishing-House-0-01.html) in Brussels with the support of [FRArt](https://www.frs-fnrs.be/fr/financements/credits-et-projets/frart)
thanks: Martino Morandi

In 1758 Carolus Linnaeus published his work Systema Naturae. Ever since, species are described for Western science by taxonomists based on a type specimen. The published scientific name and the official description of the species are then permanently associated with this type specimen. Type specimens are kept as dead material on herbarium sheets in collections worldwide, where they can be accessed by other scientists.

The herbarium of the Botanical Garden of Meise is the 15th biggest herbarium in the world. The collection counts more than 4 million specimens, and 85% of all known plants in Congo, Rwanda and Burundi. The herbarium houses more than 53000 type specimens. More than 9500 of these type specimens were collected during the Belgian colonial period in Congo, Rwanda, Burundi. Only 60 of these type specimens were described by women.
A lot of taxonomists never left their desks, like Emile De Wildeman, who was the director of the Botanical Garden of Meise from 1912 till 1931. Yet he renamed more than 3600 specimens. The third part of the ‘official’ Latin name is the abbreviation of the taxonomist’s name.

In the window of Constant you encounter a few taxonomists who renamed plants collected between 1885 and 1960 in Belgian Congo. For this occasion the virtual spirits of the type specimens who once lived a flourishing life in Congo, Rwanda and Burundi, left the herbarium to rename the taxonomists and rewild their portraits.
