title: Trees of Discomfort
type: installation
summary: This experiment is an artistic interpretation of the random forest algorithm. The random forest algorithm is a machine learning algorithm in which several decision trees are trained on a dataset and form a forest. The trees of the of forest try to predict the category of a given observation. They do this by answering a set of yes or no questions. Starting at the root of the tree they follow either the left or right branch to arrive at the next question (a knot), or at an answer (an endpoint). Once the trees have decided for themselves they vote and the forest comes to a decision.
algorithm: Random Forest
trees: Living Herbarium of Meise, Digital Herbarium of Meise, the 150 observations of the <a href= "https://armchairecology.blog/iris-dataset/">contested Iris Dataset</a>, composed by the British statistician and biologist Ronald Fisher in his 1936
humans: Guillaume Slizewicz, Gijs de Heij
language: English
on_show: Constant_V, June-Sept 2022
image: algoliterary-works/trees-of-discomfort.jpg
links: [pictures](https://piwigo.constantvzw.org/index.php?/category/4893)
license: <a href="https://gitlab.constantvzw.org/unbound/cc4r">Collective Conditions for (re-)use (CC4r), June 2021</a>
repository: <a href="https://gitlab.constantvzw.org/anais_berck/trees-of-discomfort">https://gitlab.constantvzw.org/anais_berck/trees-of-discomfort</a>
support: This experiment was a creation for the exhibition in the framework of <a href="https://constantvzw.org/site/Constant_V-An-Algoliterary-Publishing-House-0-01.html"> Constant_V</a> in Brussels with the support of <a href="https://www.frs-fnrs.be/fr/financements/credits-et-projets/frart">FRArt</a>
thanks: Martino Morandi

This experiment is an artistic interpretation of the random forest algorithm. The random forest algorithm is a machine learning algorithm in which several decision trees are trained on a dataset and form a forest. The trees of the of forest try to predict the category of a given observation. They do this by answering a set of yes or no questions. Starting at the root of the tree they follow either the left or right branch to arrive at the next question (a knot), or at an answer (an endpoint). Once the trees have decided for themselves they vote and the forest comes to a decision.

In Trees of discomfort five (decision-)trees and an announcer live on six micro-controllers (ESP-32). When an observation is announced each of the trees will try to classify the observation and explains its thinking. Once in a while, a tree expresses doubts on its behaviour and the context in which it was developed and voices its unease on botany, herbaria, the Iris dataset, and processes of classification.
