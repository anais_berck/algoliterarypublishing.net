title: Paseo por arboles de madrid
type: algoliterary book
summary: In this book, the Markov Chain algorithm simultaneously generates a poem and a walk along trees in the neighbourhood Las Letras in the centre of Madrid. 
algorithm: Markov Chain
trees: collection of trees in the neighbourhood of Barrio de Las Letras in Madrid, retrieved from <a href="http://www-2.munimadrid.es/DGPVE_WUAUA/welcome.do">Un Alcorque, un Arbol</a>
humans: Emilia Pardo Bazán
    Benito Pérez Gáldos
    Jaime Munárriz
    Luis Morell
    An Mertens
    Eva Marina Gracia
    Gijs de Heij
    Ana Isabel Garrido Mártinez
    Alfredo Calosci
    Daniel Arribas Hedo
language: Spanish
    English
published: 2021
license: <a href="https://gitlab.constantvzw.org/unbound/cc4r">Collective Conditions for (re-)use (CC4r), June 2021</a>
image: algoliterary-works/trees_madrid.jpg
publication_url: http://paseo-por-arboles.algoliterarypublishing.net
repository: https://gitlab.constantvzw.org/anais_berck/paseo-por-arboles-de-madrid
support: This book was created as part of the residency of Anaïs Berck in <a href="https://www.medialab-prado.es/en/announcements/anais-berck-algoliterary-publishing-house">Medialab Prado</a> in Madrid, granted by the Government of Flanders as part of their 'Residency Digital Culture' program. The creation happened in company of collaborators of Medialab Prado, who assisted to various workshops.

In this book, the Markov Chain algorithm simultaneously generates a poem and a walk along trees in the neighbourhood Las Letras in the centre of Madrid. Despite the impression that there are few trees in the neighbourhood, the algorithm counts 460 of them. Markov Chain was designed in 1906 by Andrey Markov, a Russian mathematician who died in 1992. This algorithm is at the basis of many softwares that generate spam. It is used for systems that describe a series of events that are interdependent. What happens depends only on the previous step. That is why Markov Chains are also called "memoryless".
