title: Forest bathing workshop
type: workshop
summary: A walk in the Forêt de Soignes with An who is a nature guide and offers sensory experiences in the forest. The idea is that the participants conduct a series of experiments and experience an encounter with one particular tree.
trees: Sonian Forest
humans: An Mertens
    students ESA St-Luc
language: English
published: November 2021
license: <a href="https://gitlab.constantvzw.org/unbound/cc4r">Collective Conditions for (re-)use (CC4r), June 2021</a>
image: algoliterary-works/forest_bathing.jpg
support: This workshop was a creation for the students of ESA St-Luc in Brussels, with the support of <a href="https://www.frs-fnrs.be/fr/financements/credits-et-projets/frart">FRArt</a>

# Forest Bathing workshop with students of ESA Saint-Luc, Bruxelles

## Introducing the context
- connecting to the macro-cosmos:
    - walk in silence
    - mute phones
    - open up to experiments, even if there can be some resistance
- Bois de la Cambre / Forêt de Soignes:
    - 5km walk
    - max 3hs

## Introducing some species
Looking at bark, leafs, fruits, specific values in use, in Celtic use
- beech
- hornbeam
- poplar
- oak
- linden
- maple
- chestnut

## Experiments
- share a memory of a tree
- choose a tree and say hello, observe its bark, look up to the leaves, down to the roots, be conscious about the wood wide web
- how does a tree manage to have its leafs change colour and fall down (this workshop was in autumn)
    - photosynthesis
    - basic exchange tree-human: oxygen & CO2
    - breathing together
- guided meditation: become an oak of 35Oys old, smell the soil in which it is rooted
- meet a tree
    - find a tree that calls you
    - greet the tree
    - ask a question following Philippine practise
    - explore your expression with your own medium (drawing/writing/photography...)
- final sharing
