title: Levenshtein Distance
type: algoliterary book
summary: The author of this book is the Levenhstein Distance algorithm, the subject is the eucalyptus tree in "Fama y eucalipto", an excerpt from Historias de Cronopios y de Famas by <a href="https://en.wikipedia.org/wiki/Julio_Cort%C3%A1zar">Julio Cortázar</a>, published in 1962 by Editorial Minotauro.
algorithm: Levenshtein Distance
trees: eucaliptus + found species in Spanish on the internet
humans: Julio Cortázar
    Gijs de Heij
    An Mertens
language: Spanish
    English
published: 2021
license: <a href="https://gitlab.constantvzw.org/unbound/cc4r">Collective Conditions for (re-)use (CC4r), June 2021</a>
repository: https://gitlab.constantvzw.org/anais_berck/levenshtein-distance-lee-a-cortazar
publication_url: https://levenshtein.local.algoliterarypublishing.net/
image: algoliterary-works/levenhstein.png
support: This book is a creation for <a href="https://www.tabakalera.eus/es/agora-cemento-codigo">ÁGORA / CEMENT / CÓDIGO</a>, an online exhibition curated by <a href="https://www.tabakalera.eus/es/lekutan">Lekutan</a>, within the programme of Komisario Berriak supported by <a href="https://www.tabakalera.eus">Tabakalera</a> in Donostia/San Sebastián, Spain.
thanks: Andrea Estankona, Jaime Munárriz, Esther Berdión

The author of this book is the Levenhstein Distance algorithm, the subject is the eucalyptus tree in "Fama y eucalipto", an excerpt from Historias de Cronopios y de Famas by <a href="https://en.wikipedia.org/wiki/Julio_Cort%C3%A1zar">Julio Cortázar</a>, published in 1962 by Editorial Minotauro. <a href="https://en.wikipedia.org/wiki/Levenshtein_distance">Levenshtein distance</a>, edit distance or word distance is an algorithm that operates in spell checkers. It is the minimum number of operations required to transform one word into another. An operation can be an insertion, deletion or substitution of a character. The algorithm was an invention of Russian scientist Vladimir Levenshtein in 1965.
