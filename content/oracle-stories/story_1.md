title: Cacao Trial
cards: Testimony
    Cacao
    Interdependence

Will be used by everyone who eats chocolate. Now people eat it in unconscious ways, some are addicted to it, very few people know about the sacral function it had in Mayan Traditions, where it is grown, and where it has been used.

Each barcode of each chocolate bar will publish a receipt that is printed when someone buys the chocolate at the supermarket. The experiences of the shamans taking care of harvesting the cacao plant will be recorded as testimonies and then printed. They will carry the experiences, so that consumers will want to read them to intensify the experience of the eating and to feel connected to the grounds that the chocolate came from.

The testimonies will soon become collector items.

What it looks like: its a database run by an NGO, that is updated monthly, a very detailed collection of histories over time and place, each story is linked to the EAN number of a specific project.