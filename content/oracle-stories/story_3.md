title: Storytellers.com
slug: storytellers
cards: Storytelling
    Order Fulfillment Apparatus
    Nothing about us without us

Anyone who likes stories, can order a storyteller on an online platform. It works like Mechanical Turk but it’s well paid - it’s physical work. A storyteller can come physically to your place, or can tell the story via video call or with a letter. An NGO organises this. They previously have interviews with each of the storytellers who are working for the platform. Storytellers can only tell from their own perspective. When you order one, the storyteller will be assigned to you based on the chosen values, language etc.

Each storyteller works in tight collaboration with 1 or more trees. Once the story is told it is published. Customers can ask the storyteller for consent if they want to re-tell the story and have to specify where they will re-distribute the story.