title: Ritual for non-stable publishing
slug: ritual
cards: Storytelling
    Ritual
    Sharing

This publishing infrastructure is a ritualistic storytelling prompt for non-stable publishing. By engaging with the different steps of ritual and telling the story to a friend, a tree, a more-than-human you will have published the work.

The different steps of the ritual:

- Feel into a story you want to share that tells of something you have learned, remembered, felt, inherited, dreamt or otherwise

- Think to yourself what are the main elements of the story, main points you want to share in the story

- Approach a friend in real life or with text or phone call, tree or more-than-human nearby

- Ask for consent to share to share your story

- If they consent, carry on; if they're unavailable, ask another being or wait until they give you a later time

- Share your story with the main points you had included, it's been published!