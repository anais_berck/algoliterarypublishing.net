title: Melt
slug: melt
cards: Testimony
    Melting
    Dismantling standards

In situation of melting due to climate change in Berlin, we will dismantle standards of publishing towards valuing testimony as a publishing infrastructure and as embodied by the transforming materials around us. It is a temporal infrastructure, only active when it snows or freezes in Berlin.

On the days when frozen things around us are melting, or defrosting, or undoing themselves, there will be a practise by humans and more-that-humans of witnessing what is released from this previously frozen materials. This melting will be the publishing!

People are encouraged to take the day off work and attempt to read the things as they're newly published as they unfreeze.