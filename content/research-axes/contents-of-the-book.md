title: Contents of the book
sort: 6

- Does reading lose its value when you can choose one million copies of a slightly different version of a book?

- Do we show the infinity of the generated copies? If yes, how?

- Does each book need the capacity to exist in infinite variations? Can we decide upon a 'static' version of a book? Is this interesting?

- How can the organisation of books be presented in a fair inclusive way? What 'fair' categories/characteristics can we think of for a first 'index' web page?

- Do we keep the plain text as a style throughout all editions of the publishing house, reflecting the materiality of code and logging? Or do we also create 'books' that look like more classic / elaborately laid-out books. A more structured lay-out seems to invite a different way of coding that is written with the lay-out in mind, for example by splitting logs into parts. But also, the code to produce the lay-out becomes part of the code, making the scripts less simple and more clearly designed.

- What is the status of an automatically generated pdf? Does it only exist in the RAM memory of the reader’s computer, and it becomes a file on a computer if the reader decides to download it? Is each copy saved on a server? Is it a good idea to respond to the field of literature, where the book is considered as something fixed? Can the immediate download be considered as a distribution strategy?

- There are several reasons to stick to the format of the pdf for automatically generated books. Would it otherwise not become something else: a media art/a webpage? Would the focus otherwise be not more on the interactivity and the experience of the visitor? Is creating pdfs a way to speak back with an object that is produced by the publishing industry and tradition? Can we take on the dress and habits of that context: we start as a website and freeze it; what is generated is static; it is a quality that it is a pdf; a way to connect to the website and vice versa?

- How can one cite automatically generated books?

- Is each generated book a unique object? It can be easily copied and redistributed. Should we talk about unique objects? Create NFTs (non-fungible tokens, a way to integrate artworks into Blockchain, wasteful by design...) as unique objects?

- How can we imagine the life of generated books? They can be present in different places: on the website, as a pdf, as a book during the walk, shared in the neighbourhood. They can become topics for a workshop: as tools and to open up algorithms to a wider audience.

- Is it an idea to open up this platform as a service? This rises other questions: what about curatorship? A publishing house is assuring quality, it is more than a Print-on-demand service.

- What about formats of the pdfs? Now they switch between A4 and US A4, so they're easy for homeprinting. Does it make sense to play with formats?

- Can we imagine linking the generation of the pdfs to a POD-platform like Lulu?
