Title: Publishing practises Varia
sort: 8

# Existing algorithmic publishing practises around Varia, Rotterdam

## Publishing Experiments

### Quilting
- combining patches: every participant makes an ASCII patch and stitiches it to another, 70x70
- is a way to make your own text based on a visual pattern
- each patch is saved as a txt file and stitched together using Python
- it demands for negociation between the different patches by adding white spaces
- all control you have is x and y
- link: <https://git.xpub.nl/XPUB/S13-Words-for-the-Future-notebooks/src/branch/master/pdf-ascii-quilting>

### Zines
- interesting zine made by Michael Murtaugh and Manetta Berends, as a way to share their thoughts and documents their classes: <https://hub.xpub.nl/sandbot/SWAAT/00/SWAAT-00.pdf>
- Jupyter notebook became a part of this publication process, in Markdown, processed using Weasyprint
- code becomes a recipe and can be contextualised using the Jupyter notebooks; code becomes textual language, doesn't need to be separated from 'natural' language; you can see the immediate outcome of pieces of code

### Nourishing Network
<https://a-nourishing-network.radical-openness.org/>
<https://a-nourishing-network.radical-openness.org/pages/documentation.html>
- each week 1 article was written, it was sent in 2-fold in a parcel together with an enveloppe; one could subscribe and would receive a parcel a week, with 1 text to keep and 1 text to send on to someone else
- the text would also be published online each Friday, and be automatically posted from the website to rss-feed/mastodon, a mailinglist could also have been an interesting option

### Continuous Publishing: Lumbung
<https://roelof.info/lumbung/>
- uses feeds/rss to bring together material from different places
- developed for Transmediale 15
- Activity Pub: the protocol behind Fediverse (Mastodon etc), each environment functions on itself, but because they speak the same language, you can bring them together automatically.

### Continuous Publishing: Multi feeder
<https://multi.vvvvvvaria.org/>
- this is a way to publish what happens in and around Varia, using the rss feed of the website and the hashtag on Mastodon
- the idea is to develop a newsletter/gazette this way
- it is a way to show that there are multiple ways to become an author, there are different ways of writing possible

## Agents for distribution

### Logbot
<https://vvvvvvaria.org/logs/>
log with examples of plain text culture: <https://vvvvvvaria.org/logs/x-y/>
- was created during [Relearn Rotterdam 2017](http://relearn.be/2019/)
- needs the xmmp protocol to chat, when logbot is in the chatroom, it logs all images/messages, is a continuous way to save things
- you create a very thin social layer between internal and public exchange
- logbot generates automatically a webpage
- code of Varia bots: <https://git.vvvvvvaria.org/varia/bots>
- it is way to publish collectively, in the moment, daily, continuous; it is the strength of algoritmic publishing, something you cannot do with print
- cfr enron mailinglist: time becomes a factor (daily, weekly)
- you can also delete things in the stream

## Tools

### Jupyter Lab
- can run on a server, but also locally
- direct & literary: combines Markdown with code, can be a nice way to publish code
- works well online, is an occasion to talk about a server (vs computer)
- could be a tool for the algoliterary publishing house if multiple makers participate
- it could be interesting to look at the differences/potentialities between Jupyeter Lab and git-repository
- notebooks can be seen as private interactive websites, you can not work collectively on 1 notebook
- binder: you upload notebook online, everyone gets access: <https://jupyter.org/binder>

### Python library for plain text
- made by Gijs de Heij & Manetta Berends
- a way to algoritmically work with language and counting, both come together
- what if lay-out is only text: canvas becomes grid of letters
- allows for freedoms  & complications
- code: <https://git.vvvvvvaria.org/mb/asciiWriter>
- example: <https://algolit.constantvzw.org/images/4/47/Data-workers.en.publication.pdf>

### Resonant publishing (ATNOFS)
- this needs more clarification: it is a next edition of the Ethertoff, where editors and graphic designers work together simultaneously
- you create a stylesheet together, can adapt the template, and you generate the pdf
- html, css, MD
- responds to the wish to make pads speak Python, based on your text you can generate new texts


## References

### On distribution models
<https://pad.vvvvvvaria.org/getting-it-out-there>
- references on experiments with different distribution models

### Bots as digital infrapunctures
<https://bots-as-digital-infrapunctures.dataschool.nl/>
- by Cristina Cochior
- you need to understand first the social codes of the environment in which you make your bot live, 'bot logic'
- algorithmic agents (bot, worker): something that 'lives' somewhere, it is a way tog et information to people, it shifts to focus from interest to publishing, you need to think about the message, to whom you want to talk, the intention with which you publish, how to avoid to overwhelm people, ...
