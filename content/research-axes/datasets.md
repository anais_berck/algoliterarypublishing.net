title: Datasets
sort: 1

*Look for purpose in the place where your deep gladness meets the world's needs (Yogi tea)*

During the FRArt-research in 2022 a thread on databases was created that could help to think about the way we want to explore experiments. Some ideas come from experience, some from questions and discussions. All threads lead to more questions and discussion.
Following the principles of the Parisian based collective Oulipo, we consider all types of texts as potential literature.

## Some overall questions could be:
    
- how can we enhance the presence of and relationship with more-than-humans and cosmovision practises? Can we create alternative narratives? Might there be *datasets* that that incorporate more indigenous, situated and folk knowledge?

- how do we create a de-antropomorphised de-centered point of view? 

- how can we work to undo the histories of violence, histories of exclusion that have already existed? How do we formulate critique on collections of structured digital data that come along with a knot of conventions and traditions from military, colonial and corporate management and control systems? What kind of datasets are *not* loaded with these kinds of problems? 


## Possible threads

### Focus on non-dominant discourses

- Attunement generator: [https://alchorisma.constantvzw.org/code-projects/attunement-generator.html](https://alchorisma.constantvzw.org/code-projects/attunement-generator.html)
How trees can help healing bad temper and anxieties of humans, following the Celtic tradition of consulting trees for daily and sacred questions.

- Indigenous crops and food databases: 
The Orona Foundation, Australia: [https://theoranafoundation.org/projects/indigenous-food-database/](https://theoranafoundation.org/projects/indigenous-food-database/)
Foodtank database: [https://foodtank.com/news/2018/08/traditional-crops-indigenous-farming/](https://foodtank.com/news/2018/08/traditional-crops-indigenous-farming/) - 30 traditional crops to celebrate indigenous farming 

### Local and real time data (pos/neg) 

link generation of text to the waves of the tides, the moon cycle, the quality of the air in certain places or local tree databases:

- Walk along the trees of Madrid: [http://paseo-por-arboles.algoliterarypublishing.net/en](http://paseo-por-arboles.algoliterarypublishing.net/en)

- Celtic map of trees by Anne-Laure Buisson: [https://alchorisma.constantvzw.org/code-projects/celtic-map-of-trees.html](https://alchorisma.constantvzw.org/code-projects/celtic-map-of-trees.html)

- Moonwalks related to the mooncalendar: [https://moonwalks.be/](https://moonwalks.be/)

### Activist stories & information

- Karin Ulmer has worked for more than 20 years with Brussels-based civil society organizations engaging in advocacy and lobbying EU institutions on policies related to sustainable food systems, agriculture and trade, land and seed rights. For the Constant worksession Alchorisma in 2019 she wrote the following text:
[https://alchorisma.constantvzw.org/essays/we-are-all-earth.html](https://alchorisma.constantvzw.org/essays/we-are-all-earth.html)
This text inspired An Mertens in the Summer of 2022 to ask Karin to prepare a list of datasets and current cases she was working on. The results are here: [https://pad.constantvzw.org/p/anais_berck_karin](https://pad.constantvzw.org/p/anais_berck_karin).
The audiofiles of Karin presenting the results are here: [https://cloud.local.algoliterarypublishing.net/s/g6fHWyiSr2MifKT](https://cloud.local.algoliterarypublishing.net/s/g6fHWyiSr2MifKT)

- Z33, Charging myths: [https://www.z33.be/en/programma/charging-myths/](https://www.z33.be/en/programma/charging-myths/)
Papers on mining Lithium in East Congo: [https://cloud.local.algoliterarypublishing.net/s/cxA8aW77Pgnmctn](https://cloud.local.algoliterarypublishing.net/s/cxA8aW77Pgnmctn)

### Literature on nature
from the book Radical Botany, by Natania Meeker

- Julien Offray de La Mettrie

- Anne Richter

- Emily Dickenson

- Dominique Brancher, Quand l'esprit vient aux plantes

- Guy de la Brosse, De la nature, vertu et utilité des plantes (1628)

- Cyrano de Bergerac, Les Etats et Empires de La Lune

- Cyrano de Bergerac, Les Etats et Empires du Soleil

- Octavia Butler, Parabole and the sawer (image of the seed): biopolitics of vegetality

- Octavia Butler, Liliths Brood: vegetal matter 

- Jamaica Kincaid, My Garden: about the question of the colonial history of the garden

### Explosive artefacts

How can you critically work with a database that is rooted in colonial, military, Western neoliberal history? How to avoid confirming it, giving it positive attention? 
As part of this research trajectory Anaïs Berck was in residency in the Botanical Garden of Meise in March 22. There we encountered texts and databases that inform their practises on a daily basis.

Some interesting texts are:

- Nagoya protocol: [https://www.cbd.int/abs/about/](https://www.cbd.int/abs/about/)

[The Nagoya Protocol on Access and Benefit Sharing (ABS)](https://en.wikipedia.org/wiki/Nagoya_Protocol) is a 2010 supplementary agreement to the 1992 Convention on Biological Diversity (CBD). Its aim is the implementation of one of the three objectives of the CBD: the fair and equitable sharing of benefits arising out of the utilization of genetic resources, thereby contributing to the conservation and sustainable use of biodiversity. It sets out obligations for its contracting parties to take measures in relation to access to genetic resources, benefit-sharing and compliance.
In short, it has now become officially illegal to travel to a country, bring back species and exploit them commercially without sharing the benefits with the country of origin.

- International Code of Nomenclature for algae, fungi, and plants: [https://www.iapt-taxon.org/nomen/main.php](https://www.iapt-taxon.org/nomen/main.php)

A fascinating book that specifies all rules, principles and uses to name plants, algae and fungi. It is updated every 4 years during an international conference.

Interviews with scientists working in the Botanical Garden of Meise reveiled a myriad of local and global databases that they use in their daily practises:

- **GBIF**: Global Biodiversity Information Facility - [https://www.gbif.org/](https://www.gbif.org/): a database from which you can extract the species list per region -- for example Belgium -- the geolocalisations for each species

- **IPNI**: International Plant Names Index - [https://www.ipni.org/](https://www.ipni.org/)

- **JSTOR** - [https://www.jstor.org/](https://www.jstor.org/): 245 institutions on African Type Specimens had free access for people from African Countries. Often people from within Africa don't have access to the data, people outside of Africa pay and it is free for people in Africa.

- **BGCI**: Botanical Garden Centers International - [https://www.bgci.org/](https://www.bgci.org/): a lot of funding goes to working on this common database

- **BICIKL** - [https://bicikl-project.eu/](https://bicikl-project.eu/): the idea is to connect different type of biodiversity data: it is an online, EU platform for researchers without programming skills

- **Plazi** - [https://plazi.org/](https://plazi.org/):a  database of literature, taxonomic treatment database with formal texts

- **Global Biotic Interaction Database** - [https://www.globalbioticinteractions.org/](https://www.globalbioticinteractions.org/): provides open access to finding species interaction data (e.g., predator-prey, pollinator-plant, pathogen-host, parasite-host) by combining existing open datasets using open source software.

- **ORCID** - [https://orcid.org/](https://orcid.org/): a unique identifier for each researcher

- **Bionomia** - [https://bionomia.net/](https://bionomia.net/): Bionomia is developed and maintained by David P. Shorthouse using specimen data periodically downloaded from the Global Biodiversity Information Facility (GBIF) and authentication provided by ORCID. It was launched in August 2018 as a submission to the annual Ebbe Nielsen Challenge. Since then, wikidata identifiers were integrated to capture the names, birth, and dates of death for deceased biologists to help maximize downstream data integration, engagement, and as a means to discover errors or inconsistencies in natural history specimen data. 

- **Scholia** - [https://scholia.toolforge.org/](https://scholia.toolforge.org/): you can find the wikidata profile and it gives you a profile based on the wikidata profile. with the number of publication per year, the topics that are concerned and then a few visualisations. where was it published, but also co-author graphs. You can see groups and see how it propagates, f.ex. masters, invasive species... If you want to do something on species, you can probably start from here. You take a species on wikidata, it's a rabbit hole !

- **Ecological Restauration Alliance of Botanical Gardens** - [https://www.erabg.org/](https://www.erabg.org/)

- **ESCONET**: European Native Seed Conservation Network - [https://cordis.europa.eu/project/id/506109/reporting](https://cordis.europa.eu/project/id/506109/reporting)

- **Darwin Core** - [https://dwc.tdwg.org/](https://dwc.tdwg.org/): Darwin Core is a standard maintained by the Darwin Core Maintenance Interest Group. It includes a glossary of terms (in other contexts these might be called properties, elements, fields, columns, attributes, or concepts) intended to facilitate the sharing of information about biological diversity by providing identifiers, labels, and definitions. Darwin Core is primarily based on taxa, their occurrence in nature as documented by observations, specimens, samples, and related information.

Datascientist Anne-Laure analysed the herbarium on the stories it does and does not tell, the variables that are not given attention to (f. ex. local names, environment where the plant has been found, description of the tree/bush/plant): [https://cloud.local.algoliterarypublishing.net/s/MBffHtzwGekNqN8](https://cloud.local.algoliterarypublishing.net/s/MBffHtzwGekNqN8)
