title: Algorithms as Authors
sort: 4

- How can we consider our geo-political and body-political position when designing, building, researching and theorizing about computing?

- How can we embrace the 'decolonial' option: thinking through what it means to design and build the systems that we propose to produce with and for those situated at the periphery of the world system. This involves thinking with trees as entities that have been under cared for and are literally on fire due largely also to the effects of colonialism and subsequently global climate change. How can we respectfully engage with trees when creating a publishing house?

- How can we unfold coding practises: understand their structures, the histories and contexts they are embedded in and the radical processes they execute. What if algorithms were committed to a more equal world and a healthier planet? What if algorithms tried to form a symbiosis with trees?

- Can we further develop the methodology that was developed during Algolit sessions? Leading to understanding the code of existing models:
- trying out scripts
- comment scripts 
- print out each line of code in the terminal
- playing with different input and output
- adapting scripts to your needs 
- refining the tool

Three principles prevail: 
    a) move techniques to another context, for which they were not designed; 
    b) do not try to optimise techniques, but create interfaces, visualisations, code comments, so that they manage to express themselves in some way; 
    c) choose the level of understanding, each model being composed like an onion. It is possible, for example, to run a model as it is, to focus on the art of approximation (statistics, algebraic formulas) or to simply make parts of models without using code, to explore and perform them physically or metaphorically (Dataworkers, 2019), because "an algorithm needs to be seen in order to be believed.” (Knuth 1997, page 4). 

It is a collective process. No person in the space knows all the answers to the questions that emerge. In this kind of process, insiders can become outsiders, and outsiders can become insiders. 

Some examples of commented scripts:   
- word2vec:
Commented script, with outputs of different steps as text files: [https://gitlab.constantvzw.org/algolit/algolit/-/blob/master/algoliterary_encounter/word2vec/word2vec_basic_algolit_tensorflow-0.12.1.py](https://gitlab.constantvzw.org/algolit/algolit/-/blob/master/algoliterary_encounter/word2vec/word2vec_basic_algolit_tensorflow-0.12.1.py)

- random forest: [https://gitlab.constantvzw.org/anais_berck/random_forest/-/blob/main/commenting_code_model/random_forest_model_altered.py](https://gitlab.constantvzw.org/anais_berck/random_forest/-/blob/main/commenting_code_model/random_forest_model_altered.py)

- perceptron: [https://gitlab.constantvzw.org/algolit/algolit/-/blob/master/2019/Simple_Perceptron_including_Bias_Processing/Simple_Perceptron_including_Bias_Processing.pde](https://gitlab.constantvzw.org/algolit/algolit/-/blob/master/2019/Simple_Perceptron_including_Bias_Processing/Simple_Perceptron_including_Bias_Processing.pde)
