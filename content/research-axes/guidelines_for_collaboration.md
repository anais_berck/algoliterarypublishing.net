Title: Guidelines for Collaboration
sort: 10

**Note**: These guidelines are adapted from the Constant’s Collaboration Guidelines: <https://constantvzw.org/wefts/orientationspourcollaboration.en.html>
Anaïs Berck was launched by An Mertens in October 2019. The idea of the collective grew out of An's 13 year long commitment as a member of the coordinating team and activities of Constant. Therefore Anaïs Berck shares the philosophy of Constant. Constant is a non-profit, artist-run organisation based in Brussels since 1997 and active in-between art, media and technology. Constant develops, investigates and experiments. Constant departs from feminisms, copyleft, Free/Libre + Open Source Software and works on those vectors through an intersectional perspective. More about Constant: <https://constantvzw.org/site/-About-Constant-7-.html>.

# COLLABORATION GUIDELINES
## for the residency of Anaïs Berck in September/October 2022

### Commitment
Every participant of this residency is committed to the focus of the residency as mentioned in the open call:
During the residency participants develop algoliterary publications. These are publishing experiments with algorithms and literary, scientific and activist datasets about trees and nature. We ask ourselves: who and what is excluded, made invisible or exploited in the existent representations, discourses, tools and practices? How can we restore their presences in histories and storytelling? How can we heal and transform ourselves, our tools, our practices, our relationships to the world, our legacies? How can we help to destabilize the centrifugal force in botany, computation and publishing? How can we make books, databases, algorithms visible as objects of doubts and how to go beyond their established forms?
In case of doubt it will be important to look at the bigger picture: does your proposal for the algoliterary publishing house correspond to stories you want to tell in order to contribute to a healthier relationship with Mother Earth and fight against climate change? And to whom are these stories addressed?

Anaïs Berck is committed to environments where possible futures, complex collectivities and desired technologies can be experimented. The spaces that we initiate are therefore explicitly opposed to sexism, racism, queer antagonism, ableism and other kinds of oppression. Our positioning is one of risk-taking and trial and error in which rigour and critique meet with humour, insecurity, tension, ambiguity and mistakes. Fearless, brave environments empower radical complexity.

Departing from feminisms means for Anaïs Berck to be attentive to the sometimes generative, often oppressive arrangements of power, privilege and difference. We understand these arrangements to be related to gender and always to intersect with issues of for example class, race and ability. Finding ways to come to terms with the long colonial history of computation and botany, the way technology impacts ecology, and the relations between them, deserves our ongoing attention.

Anaïs Berck acknowledges that there are asymmetries and inequalities present in any group of human beings. We acknowledge that we all carry wounds and blind spots with us and that these can lead to tension. Therefore we encourage people to be present with a welcoming, listening and questioning, rather than a judging attitude; being conscious that Anaïs Berck attempts to operate from inclusivity rather than exclusivity. We want our work to take very different human beings and their own universes into account but also to include historical and future other-than-human agents. This means to keep challenging our assumptions and to welcome being challenged about ways we might be able to address the intersections of privilege, power, history, culture, inequality, pain, and injustice.

Every day some other participant will take on the role of the contact person. If we are feeling unsafe or seeing someone who seems in distress, we can immediately find the the contact person. They will do their best to help, to address the issue and/or to find the correct assistance if relevant/necessary. Information will be handled with sensitivity.

The past years have confirmed that governmental laws/regulations/measures have often been out of sync with actual needs… so be ready to re-discuss collectively how to relate to these, we encourage to proactively express discomfort / sense it with the others around you.

Anaïs Berck supports Free Culture because it is a way to acknowledge that culture is a collective effort that deserves to be shared. There is no tabula rasa, no original author; there is a genealogy and a web of references though. When it comes to technology, we think Free Software can make a difference because we are invited to consider, interrogate and discuss the technical details of software and hardware, or when we want to engage with its concepts, politics and histories. Over the last years, we have come to the realisation that being affirmative of Free Culture has to come with more critical considerations. We want to take into account the links of Open Access ideology to colonial extractivism which can obstruct the imagination of complexity and porosity. In addition we want to take into account the rights to opacity in access and transmission of knowledge, especially in regard to marginalized communities. Constant has written a license which tries to address these considerations. We are experimenting with this license and now distribute all Anaïs Berck’s work under the Collective Conditions for (Re)use license: https://constantvzw.org/wefts/cc4r.en.html.
The experiments as well as the code you will develop during this residency will only be published with your consent, mentioning Anaïs Berck - An Algoliterary Publishing House, and all the humans, trees and algorithms that are part of the experiment. At the end of the residency we will decide what happens with the dedicated server and all materials it contains.

### Collaboration Guidelines
We wrote a short version of the guidelines. Against the wall you can find a longer version as well. We invite you to read that one as well when you find some time.

Residencies are intensive transdisciplinary situations to which participants from very different backgrounds contribute. Because of the intensity of exchanges and interactions during residencies, there can be moments of disagreement and discomfort. These moments need to be acknowledged and discussed, within your own limits.
Even if some of the below guidelines sound obvious, we have experienced that being together can be complicated. We have written these guidelines to think of ways to be together comfortably and attentively. Furthermore, by addressing the guidelines as part of each residency, we hope to create dynamic ways to keep training our abilities to expand and strengthen braver spaces. The guidelines are meant to create potentiality for all, and sometimes this is done by restricting the space taken by some.

### Collaboration Guidelines - Short Version
Collaborators with and within Anaïs Berck take the following into account:

    • If you feel you're judging, leave the room and come back.
    • Everything you do from the heart is good.
    • If you prefer to do nothing, stay present and sustain the group energy.
    • Enjoy the process, don't be obsessed by doing it 'right'. There is no success or failure. It is the process that counts.
    • We're all learning to stay with the trouble in the complexities of climate change, injustices, paternalism, exploitations, privileges.
    • All you need is to feel the willingness to show up and be present.
    • Refusing and deconstructing sexism, racism, queer antagonism, ableism, ageism and other kinds of oppression.
    • Leaving physical, emotional and conceptual room for other people.
    • Respecting other beings, present or not, human or more-than-human.
    • Caring for physical and digital environments.
    • Avoiding to speak for others.
    • Try to not be solely guided by your preconceptions.
    • Taking time to actually listen.
    • Asking before assuming.
    • Welcoming multiple processes of (un)learning. The exchange of information, experience and knowledge comes in many forms.
    • Accepting differences. Appreciating divergence in pace, points of view, backgrounds, references, needs and limits.
    • Recognizing that words and ways of speaking impact people in various ways.
    • Caring for language gaps. This is a multi-lingual environment.
    • Using Free, Libre and Open Source software whenever possible.
    • Asking for explicit consent before sharing photographs or recordings on proprietary social networks.
    • The default license for all material and documentation is a Collective Conditions for (Re)use license: <https://constantvzw.org/wefts/cc4r.en.html>
    • Knowing that taking all of the above into account is sometimes easier said than done.
    • Harassment is unacceptable and will not be tolerated during any Anaïs Berck event, meeting or gathering. See the full guidelines against the wall for what we understand by harassment.

If we run into conflict with one of these guidelines, or when we see that others are flagging our behaviour:
    • we do not fuel the conflict.
    • we speak with each other.
    • we step out of the room and breathe.
    • we apologise.
    • we come back with a renewed engagement to collaborate.
    • if we continue to transgress the guidelines, we will be asked to leave.
