title: Book as form
sort: 5

- When algorithms produce a book, they go beyond its established form. Index ‘pages’ can run over a thousand different pdfs, page numbers become a technical artefact for a digital book. Furthermore, computer scripts can generate an infinite amount of text, but given some graphical lay-out elements in the code, they can also generate an unlimited amount of pdfs, of which each one can be different. They are capable of generating so much text that it becomes noise, too big to grasp, impossible to read in a lifetime and potentially useless.

- If we consider that the narratives of the algorithms are important to teach us about their functioning, then what they produce matters. What is printed also matters. We ask ourselves what kind of decision making methodologies we can invent to deal with their abundance and make their work legible for human beings.
