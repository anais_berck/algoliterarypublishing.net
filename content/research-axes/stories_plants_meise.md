title: Stories and histories of plants in Meise
sort: 9

In the framework of the residency in Meise we had the opportunity to talk to different people working in the institution.

We are very grateful for the time and energy of Ann Bogaerts - Head Herbarium, Koen Es - Head Education Department, Henry Engledow – Databasemanager, Sofie De Smedt - Projectleader digitalisation herbarium, Wim Tavernier - Wood expert, Denis Diagre – Manager archives, Sofie Meeus – Datasteward & citizen science, Quentin Groom – Computer scientist biodiversity, Filip Vandelook - Head Seedbank, Marc Reynders, Scientific manager of the living collections.

What follows are a series of stories of plants. The stories relate to fragments of the conversations that caught our attention. Some stories weave in elements from different conversations and all stories are the result of a lot of browsing through the agglomerate of online botanical databases we discovered in Meise.

**[Doliocarpus major J.F.Gmel. →]({filename}/pages/doliocarpus_major.md)**
