title: Publication forms
sort: 3

The research and activities of Algolit and Anaïs Berck have lead to different publication forms.
They grew somewhat organically, informed by deadlines, budget, materials and inspired by other works.
This list is an invitation to think through what publication forms can be, and what other forms we can think of.

## screens
### terminal
Text in the terminal can be shown in different colours, indentations can make the text breathe.
The background colour can be adapted. The terminal functions as a plain text screen.
Examples:
- **2019, Grafting trees**
  Description: [http://anaisberck.be/grafting-trees/](http://anaisberck.be/grafting-trees/)
  ![](https://gallery.constantvzw.org/var/resizes/Data%20Workers/DSC00481.JPG?m=1674815268)
- **2017, We are a sentiment thermometer**: [https://algolit.net/index.php?title=We_Are_A_Sentiment_Thermometer](https://algolit.net/index.php?title=We_Are_A_Sentiment_Thermometer)
  ![](https://gallery.constantvzw.org/var/resizes/Algoliterary-Encounter/P1050688.JPG?m=1674815257)

### browser
Using Javascript, node.js, Weasyprint or Paged.js, publication results can easily be shown in a browser. Pdfs can be automatically generated.
Examples:
- **2019, Tf-idf**: [https://algolit.net/index.php?title=TF-IDF](https://algolit.net/index.php?title=TF-IDF)
  ![](https://gallery.constantvzw.org/var/resizes/Data%20Workers/P1010453.JPG?m=1674815269)
- **2019, Levenhstein Distance reads Cortázar**: [http://anaisberck.tabakalera.eus/en](http://anaisberck.tabakalera.eus/en)
- **2021, Walk along the trees of Madrid**: [http://paseo-por-arboles.algoliterarypublishing.net/](http://paseo-por-arboles.algoliterarypublishing.net/)
- **2021, Michel, Cassandra, Google and the others**: [http://etraces.une-anthologie.be/](http://etraces.une-anthologie.be/)
  ![](https://piwigo.constantvzw.org/_data/i/upload/2021/11/26/20211126110407-73766dd8-me.jpg)

### e-paper
  - 2022, Trees of Discomfort: [https://studio.guillaumeslizewicz.com/post/697443830362996736/trees-of-discomfort-trees-of-discomfort-is-an](https://studio.guillaumeslizewicz.com/post/697443830362996736/trees-of-discomfort-trees-of-discomfort-is-an)

## printers
### receipt printer
- **2020, Une Anthologie**: [https://www.anaisberck.be/an-anthology/](https://www.anaisberck.be/an-anthology/)
![](algoliterary-works/Une_anthologie_2_low_l.gif)

### plotter
- **2016, The Death of the Authors - a botopera**: [https://publicdomainday.constantvzw.org/#1943](https://publicdomainday.constantvzw.org/#1943)
![](https://gallery.constantvzw.org/var/resizes/Botopera-at-PoetryFest2015/IMG_3539.JPG?m=1674815259)

## objects
### robot Zora
Zora is a robot that was developed to assist people in rest homes and hospitals. In Muntpunt, a public library in Brussels, Zora serves during public moments. For the occasion of Public Domain Day, Zora could be part of The Algoliterator, reciting final texts people made the Algoliterator write.
- **2017, The Algoliterator**: [https://algolit.net/index.php?title=Algoliterator](https://algolit.net/index.php?title=Algoliterator)
![](https://algolit.net/images/thumb/c/c3/MG_3050_algoliterator4.jpg/399px-MG_3050_algoliterator4.jpg)

## paper on tables
- **2017, word-2-vec**: word embeddings are a neural network technique in which words of a text pass 13 stages before calculations can begin, this installation visualised the different stages using printed stacks on 13 different tables:
[https://algolit.net/index.php?title=Word2vec_basic.py](https://algolit.net/index.php?title=Word2vec_basic.py)
![](https://algolit.net/images/thumb/2/29/Word-embeddings-steps-algoliterary-encounter.JPG/300px-Word-embeddings-steps-algoliterary-encounter.JPG)

## wiki-to-print
We collectively wrote a catalogue for exhibitions using a Mediawiki. The graphic designer developed scripts that could transform the content of the wiki into a printed object.
- **2019, Dataworkers catalog**: [https://algolit.net/index.php?title=Data_Workers_Publication](https://algolit.net/index.php?title=Data_Workers_Publication)
- **2017, Algoliterary Encounters**: [https://algolit.net/index.php?title=Algoliterary_Encounters](https://algolit.net/index.php?title=Algoliterary_Encounters)

## tote bag
- **2019, The Book of Tomorrow in a Bag of Words**: [https://algolit.net/index.php?title=The_Book_of_Tomorrow_in_a_Bag_of_Words(https://algolit.net/index.php?title=The_Book_of_Tomorrow_in_a_Bag_of_Words)]
![](https://gallery.constantvzw.org/var/resizes/Data%20Workers/DSC00532.JPG?m=1674815268)
