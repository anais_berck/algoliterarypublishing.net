title: About trees
sort: 2

- How can we give a voice to trees in our creation and decision processes? What methodologies can we find to somehow activate their presences, consult them, *listen* to their points of view?

- Does it still make sense to print books on paper? And what is the ecological footstep of creating and reading a digital book? Is it possible to organise an equitable book publishing activity in which trees have a say in content and form? Is there a way to publish books in a way that is respectful of trees and nature? What form would this take?

- How can we find materials that translate in a metaphorical way the reasoning of the algorithm-author. Does it make sense to work with *universal* databases on trees and nature? What about a situated way of working, where we create our own databases including all kind of subjective data, like vernacular names, stories, curing recipes, contextual data from sources such as temperature, light, moisture related to specific trees? How do we deal with existing data architectures, colonial herbaria, legislative texts, philosophy, literature? How does the choice of data affect the representation of trees?

- Since programming code - the dialogue with algorithms - is often a very draining activity that can generate a lot of stress, it might be interesting to lead experiments to see if and how conscious visits to the forest could influence the writing of code.
