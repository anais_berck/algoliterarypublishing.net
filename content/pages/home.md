title: An Algoliterary Publishing House
slug: home
status: hidden
save_as: index.html
<!-- template: home -->

This project by [Anaïs Berck](https://www.anaisberck.be/) proposes to explore the notion of a publishing house in which the authors are algorithms, presented with their contexts and codes; and in which the content of the books seeds with trees and nature.

Anaïs Berck is an artist name that stands for a collaboration between humans, algorithms and trees. As a collective Anais Berck opens up a space in which human intelligence is explored in the company of plant intelligence and artificial intelligence.

This research is organised with the support of [FRArt/Art & Recherche](http://www.art-recherche.be), and in partnership with [ESA St-Luc Brussels](http://www.stluc-bruxelles-esa.be/), [ESA La Cambre Brussels](http://www.stluc-bruxelles-esa.be/), [Botanic Garden Meise](https://www.plantentuinmeise.be/en/home/), [Villa Empain](https://www.villaempain.com/), the [Royal Library of Belgium](https://www.kbr.be/en/) and the [Bibliothèque Nationale de France](https://www.bnf.fr/fr). It was initiated in June 2021 during a residency at [Medialab Prado](https://www.medialab-matadero.es/) in Madrid granted by the Vlaamse Overheid as part of their 'Digital Culture Residencies' program.


**[Read more →]({filename}/pages/about.md)**