title: Reader

The following authors and texts are inspiring sources to think about algoliterary publishing with more-than-human agents.

- Layla F. Saad, Me & White Supremacy, 2018, [https://www.meandwhitesupremacybook.com/the-journal](https://www.meandwhitesupremacybook.com/the-journal)

- Elodie Mugfreya Celebration and Omission, Constant, 2019 [https://diversions.constantvzw.org/wiki/index.php?title=Afrique_aux_noirs](https://diversions.constantvzw.org/wiki/index.php?title=Afrique_aux_noirs)

- Mondotheque & Algolit, Paul Otlet, an Omissum, Constant, 2020 [https://diversions.constantvzw.org/paul-otlet-an-omissum.html](https://diversions.constantvzw.org/paul-otlet-an-omissum.html)

- International Code of Nomenclature for algae, fungi, and plants, [https://www.iapt-taxon.org/nomen/main.php](https://www.iapt-taxon.org/nomen/main.php)

- Malcom Ferdinand, Decolonial Ecology, Polity Press 2021

* Anna Tsing, Feral Atlas, [https://feralatlas.supdigital.org/poster/coffee-rust-spreads-together-with-coffee-plantations]( https://feralatlas.supdigital.org/poster/coffee-rust-spreads-together-with-coffee-plantations)

- Ted Striphas, The late age of print, Columbia University Press, 2011.
[Extracts]({static}/pdfs/1_Late_Age_of_Print.pdf)

- Outi Laiti, Old Ways of Knowing, New Ways of Playing: The potential of collaborative game design to empower Indigenous Sámi, Acta electronica Universitatis Lapponiensis 302, 2021.
[Extracts]({static}/pdfs/2_Old-Ways-of-Knowing.pdf)

- Sheila Watt-Cloutier, The Right to Be Cold, One Woman’s Fight to Protect the Arctic and Save the Planet from Climate Change, University of Minnesota Press, 2015
[Extracts]({static}/pdfs/3_The-Right-to-be-cold.pdf)  

- Amy Verhaeghe, Ela Przybylo & Sharifa Patel, On the Im/possibilities of Anti-racist and Decolonial Publishing as Pedagogical Praxis, Feminist Teacher, University of Illinois Press, Volume 28, Numbers 2-3, 2018, pp. 79-90.
[Extracts]({static}/pdfs/4_Anti-Racist_Decolonial-Publishing-House.pdf)

- Jan Diwata, Decolonizing the Cacao Ceremony, Phillipino Tree Communicating Practices Collection shared by Peachie Dioquino-Valera, <https://www.jandiwata.com>
[Text]({static}/pdfs/5_Peachie-Dioquino-Valera_The-Philippines_combined.pdf)
