title: Guidelines
slug: guidelines--short

This research included trees as active beings in the process, looking for ways to give them a 'voice'. Thanks to that approach, we have been introduced to the darker chapters in the history of tree and plant populations. We were given time to enter that darkness, feel the discomfort and collective trauma, and find tools to deal with it.
