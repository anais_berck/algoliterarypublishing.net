title: Oracle for Consentful Publishing Infrastructures
slug: oracle
save_as: oracle.html
template: oracle

This oracle is a hands-on card deck in development. It is designed to help us collectively envision and share ideas for what consentful publishing infrastructures might be... or might not. It was inspired by [The Oracle for Transfeminist Technologies](https://www.transfeministech.codingrights.org/).

The game is a way to explore the concept of an algoliterary publishing house. As Anaïs Berck represents a collective of humans, trees and algorithms, the algoliterary publishing house will function within this collective. This means that we will have to rethink all aspects of publishing in collaboration with all entities involved. This raises a series of questions, like: how can questions around ethics and ecology be thought alongside print or online publishing? What does consent mean when working with trees, algorithms and more than humans?

At the right you can read stories that are result from consulting the Oracle for Consentful Publishing Infrastructures.

![A green card with the words value and cacao on it. Cacao beans are partially visible.]({static}/images/2-Value-Cacao.jpg)
The Values, Objects and Situations of the Oracle have been harvested while reading texts from the [reader]({filename}/pages/reader.md) and extracting elements from the texts. The deck is unstable and still in development.

[↓ Download the deck (~35mb)]({static}/images/Oracle_for_Consentful_Publishing_Infrastructures.zip)  
[↓ Download the rules]({static}/pdfs/Rules_oracle_for_consentful_infractructures.pdf)


![A wooden box is shown. Wood burned onto it are the words ‘Oracle for Consentful Publishing Infrastructures’]({static}/images/final_box_madrid.jpg)
We also created a box that can hold the cards and a zine with texts from the [reader]({filename}/pages/reader.md).

Download the plan of the box in [pdf]({static}/pdfs/plan_lasercutter_Oracle.pdf), [dxf]({static}/pdfs/plan_lasercutter_Oracle.dxf) or [3dm]({static}/pdfs/plan_lasercutter_Oracle.3dm) to be executed with a lasercutter.




