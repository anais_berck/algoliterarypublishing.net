title: Activities organized by the publishing house
save_as: activities.html

The research is organised around different activities.
It started as a [FRArt/Art & Recherche](http://website.art-recherche.be/en/database/COL.PROJ.30/COL.SEQ.121) research project from October 2021 till October 2022.

- **Essay, St Etienne, FR, [ESAD Random (Lab)](http://www.randomlab.io/), May-June 23: Digital Tools for Creative Collaboration, [Holding space for discomfort in collective work](https://ourcollaborative.tools/article/holding-space-for-discomfort-in-collective-work)**

- **Art work, Chalon-sur-Saône, FR, June 23: ‘Rewilding Specimens’ in book [Visual Worlds](https://visualworlds.net)**

- **Lecture, Paris, FR, [Ecole de droit de Sciences Po](https://www.sciencespo.fr/ecole-de-droit/fr/actualites/semaine-doctorale-intensive.html), Semaine Doctorale Intensive, 13 June 23: Strategies for creating a safe space in collective research work**
on the invitation of dr Severine Dusollier (Ecole de Droit de Sciences Po)

- **Workshop, Stuttgart, D, Merz Akademie, April 23: Rules rule**
On the invitation of Joost Bottema, a workshop on creating a situated database of trees, bushes and other more-than-human beings who live near the Akademie. Students will explore a series of algorithms that offer rules to create, using the material of the situated database.

- **Lecture, Louvain-La-Neuve, UCL, April 23: [Pratique du numérique en sciences humaines](https://uclouvain.be/cours-2022-lfial2020)**
on the invitation of dr Isabelle Gribomont (ILC, UCLouvain)

- **Lecture, Lyon, FR, 2-3 March 23:  Fiction & données : une perspective éco-décoloniale**
in collaboration with Isabelle Gribomont (ILC, UCLouvain), in the symposium ‘Fiction and data’ at the university Jean Moulin Lyon 3, organised by Anais Guilet (LLSETI, USMB), Bertrand Gervais (Figura, UQAM) and Gilles Bonnet (Marge, Lyon3).

- **Lecture, Brussels, LUCA School of Arts, March 23: Narrative Experiences**
on the invitation of Guillaume Slizewicz in the program Media and Information Design and Graphic Storytelling.

- **Workshop, Online, London, [COPIM](https://www.copim.ac.uk/), March 23 – Que(e)rying Wikidata**
Workshop with An Mertens and Z Blace inspired by the workshop at XPUB in Piet Zwart Institute (see below).

- **Lecture & workshop, Brussels, Ecole de Recherche Graphique, Feb-March 23: [Cultures numériques](http://culturesnumeriques.erg.be/)**
On the invitation of Stéphane Noël, Lucille Calmel and Ludivine Loiseau, An Mertens will give a presentation and a series of 3 workshops in company of Gijs de Heij around Que(e)rying Wikidata

- **Workshop, Brussels, ESA La Cambre, Dec 22: Literary Python**
On the invitation of Valéry Cordy, An Mertens gave another series of workshops in ‘CASO Arts Numériques’ in La Cambre, to teach students how to make literary works using the programming language Python.

- **Workshop, Rotterdam, NL, XPUB PZI Institute, November 22: Que(e)rying Wikidata**
Workshop by Michael Murtaugh, director of the Experimental Publishing (XPUB) program at PZI Institute of Rotterdam, Gijs de Heij, queer artivist Z. Blace and An Mertens.

- **13 Oct - 16 Oct 2022**: Residency 'Algoliterary publications', Meyboom studio (tbc), Brussels, with Ahnjili ZhuParris, Ipek Burçak, Gülce Padem, Livia Diniz, Isabelle Alvers, Mara Karagianni, Brendan Howell, Doriane Timmermans, Gijs de Heij, An Mertens

- **16 Sept - 18 Sept 2022**: Residency 'Algoliterary publications', Constant studio (tbc), Brussels, with Ahnjili ZhuParris, Ipek Burçak, Gülce Padem, Livia Diniz, Isabelle Alvers, Mara Karagianni, Brendan Howell, Doriane Timmermans, Gijs de Heij, An Mertens

- **9 September 2022**: Workshop [An Algoliterary Publishing House 0.1](https://constantvzw.org/site/An-Algoliterary-Publishing-House-0-1-Workshop-finissage.html), Constant, Brussels, with Guillaume Slizewicz, Gijs de Heij, An Mertens

- **1 July - 18 September 2022**: [An Algoliterary Publishing House 0.1](https://constantvzw.org/site/Constant_V-An-Algoliterary-Publishing-House-0-01.html), Constant_V, Brussels, with Guillaume Slizewicz, Gijs de Heij, Brendan Howell, An Mertens

- **21 June - 1 August 2022**: [Open call for paid residency](https://www.anaisberck.be/open-call/)

- **21 March - 2 April 2022**: Residency ‘Bud spotting’ in the Botanical Garden of Meise, with Gijs de Heij, Loren Britton, Guillaume Slizewicz, An Mertens

- **December 2021**: Discussion with Manetta Berends about the publishing practises around [Varia](http://varia.zone) in Rotterdam

- **November/December 2021**: [Literary Python for Beginners](https://www.anaisberck.be/literary-python-for-beginners/), CASO Arts Numériques, ESA La Cambre, Bruxelles

- **26 Oct - 29 Oct 2021**: Workshops with students of ESA Saint-Luc Bruxelles [Download description workshops]({static}/pdfs/workshops_st_luc_description_FR_EN.pdf)

- **25 Oct 2021**: Seminar 'On human, plant and artificial intelligence' in ESA Saint-Luc Bruxelles, with lectures by Jara Rocha (SP), Outi Laiti (FI), Stephan Kempelman (BE) and Nathalie Grandjean (BE). [Download the full program]({static}/pdfs/seminar_st_luc_description_FR_EN.pdf)

- **18 Oct - 30 Oct 2021**: Residency ‘Fruit Rains’ in Villa Empain, Brussels, with Gijs de Heij, Loren Britton, Guillaume Slizewicz, An Mertens
