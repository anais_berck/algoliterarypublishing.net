title: Algoliterary Experiments
template: algoliterary-works
save_as: algoliterary-works.html

These algoliterary experiments have been inspired by methodologies that were developed during Algolit sessions in Brussels. Algolit is a research group on free code and texts: <https://algolit.net>.
