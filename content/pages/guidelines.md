title: Guidelines
slug: guidelines

This research included trees as active beings in the process, looking for ways to give them a 'voice'. Thanks to that approach, we have been introduced to the darker chapters in the history of tree and plant populations. We were given time to enter that darkness, feel the discomfort and collective trauma, and find tools to deal with it.

## Handles

### Short
When one collaborator decided to leave this research project in Spring 22, the effect was an existential crisis and discomfort amongst the remaining members of the team. As an attempt to find solutions An Mertens spoke to friends and artist colleagues. These informal conversations learned her that explosions and crises are very common in art projects dealing with decolonial theory or materials. She learned about situations in art centres, universities, musea, archives, some far away, some very nearby.
These discussions helped a lot in making this page public, as a way to share references, thoughts and experiences, and as an invitation to keep working through difficult discomforting situations, so that maybe one day we can say that we have the deep collective trauma behind us.

### Thankful references
We are very grateful for the generous documentation organised by [Teaching to Transgress](http://ttttoolbox.net/), a programme taking place in ERG and 2 other EU schools from 2020 till 2022. The project was a collective research and study setting out to develop insights and tools to make higher education institutions more inclusive, with an aspiration to create intersectional feminist and decolonial pedagogies with a focus on the arts. The project encountered a lot of complexities and discomfort.
At the end of the trajectory two interesting things happened. They invited the decolonial cultural worker [Teresa Cisneros](http://ttttoolbox.net/Teresa_Cisneros:_Undoing_the_institution:_a_person_at_a_time.xhtml) to help to review and reflect on the process of this project from multiple perspectives. With her they explored the pain points of the project and shared the insights that they took from this experience so that others can learn from them for collaborative projects in the future. The summary is [here](http://ttttoolbox.net/Reflections::index.xhtml#the-reflection-workshop-2021).
Laurence Rassel, director of ERG, was so generous as to share the manual that Teresa Cisneros developed for the staff of the Welcome Gallery in London, so we could learn about our blind spots and privileges.

Another important reference is the book [*Me and White Supremacy: Combat Racism, Change the World, and Become a Good Ancestor*](http://laylafsaad.com/meandwhitesupremacy) by [Layla Saad](http://laylafsaad.com/), an East African, Arab, British, Black, Muslim woman living in Qatar. She offers an impressive 28-day process that she calls a "personal anti-racism tool" designed to teach those with white privilege how systemic racism works and how they can stop contributing to white supremacy in the world.
The book began a few years ago as a 28-day Instagram challenge, evolved into a free PDF digital workbook and has now become a New York Times bestseller. Yeah!

### Generous colleagues

On August 23, Manetta Berends, Christina Cochior, An Mertens and Femke Snelting met in Varia in Rotterdam, to speak about the tensions in the project and about ways we could address whiteness and privilege in collective work. We organised the meeting because what happened in the Algoliterary research was touching our many interlocking collaborations and friendships, and we wanted to take responsibility together. We spent some time thinking through what could have been different, sharing experiences within Varia and Constant with guidelines and working with buddies for example. We felt that a frame-check could have helped to detect differences in positionality in the project early on. Such a frame-check could probably be useful for many other small-sized projects as well. We also observed that it is risky when only one person is responsible for coordination. When that role is rotated, issues can be brought up without immediately becoming personal and it makes space for continuous reflection and distributed response. We ended the meeting with a commitment to keep exchanging experiences and cases, to learn and unlearn with and from each other.

## Experiences
In the residency on 'Algoliterary Publications' that took place in the fall of 2022, An Mertens changed some of the conditions for collaborating, based on the conversations and readings listed above. She adapted the Constant [guidelines for collaboration](https://pad.local.algoliterarypublishing.net/p/residency_collaboration_guidelines) and proposed to collectively read them out loud at the beginning of day 1.
Each participant was assigned a buddy.

![We see the feet of people sitting on meditation cushions around 3 lit candles and a series of drawings of trees]({static}/images/sitting_corner.jpg)

As part of this framework An also organised a sitting corner. Every day we would start and end the day there with guided meditations and intimate sharing rounds. Being in the centre of the city, this was also the space where we made our personal connections to trees.
On the first day An asked every participant to present a tree that is of importance in their lives, to draw it and to describe it to each other. In the following days An would gradually guide the collaborators towards a closer connection to these trees. By the end of this first part of the residency An assigned the tree collection as the advisory board of the residency, that could be consulted with any kind of question during the rest of the days together.
The two sharing moments a day in this sitting corner allowed to make place for tensions that had come up during the day, mainly related to white privilege and dominant positions. Discomfort could be shown, expressed and transformed here. The participants expressed their gratitude for this space, for the guidelines, for the care and follow-up. At the end of part 2 of the residency, they also proposed to continue the intimate space in a chatroom hosted on an independent server, where no company or institution is monitoring the conversations. 

## To be continued
The creation and application of these different tools are an important part of this research trajectory. And the work of unlearning and being aware of blind spots is life long. Therefore, this page might be updated with more experiences later.
