from typing import List
from datetime import date, datetime
from markdown import Markdown
import re
from slugify import slugify as do_slugify

def find_category (categories, slug):
  for (category, articles) in categories:
    if category.slug == slug:
      return (category, articles)
  
  return (None, None)

def get_category_articles (categories, slug):
  category, articles = find_category(categories, slug)

  if category:
    return articles
  else:
    return []

def display_if_set (value, label):
  if value:
    if isinstance(value, (list, tuple)):
      value = ', '.join(value)

    if isinstance(value, (date)):
      value = date.strftime(value, '%d-%m-%Y')

    if isinstance(value, (datetime)):
      value = date.strftime(value, '%d-%m-%Y %H:%i')

    if value.startswith('http://') or value.startswith('https://'):
      value = '<a href="{0}">{0}</a>'.format(value)

    return '<dt>{}</dt><dd>{}</dd>'.format(label, value)
  else:
    return ''


# Initialize a markdown parser based on the global
# settings of pelican.
def make_markdown_filter ():
  # md = Markdown(**self.settings['MARKDOWN'])
  # settings = self.settings['MARKDOWN']
  # settings.setdefault('extension_configs', {})
  # settings.setdefault('extensions', [])
  # for extension in settings['extension_configs'].keys():
  #     if extension not in settings['extensions']:
  #         settings['extensions'].append(extension)
  # if 'markdown.extensions.meta' not in settings['extensions']:
  #     settings['extensions'].append('markdown.extensions.meta') 

  md = Markdown()
  
  def filter (value):
    return md.convert(value)

  return filter

# def markdown_filter(value):
#     extensions = ["extra", ]

#     return mark_safe(markdown.markdown(value,
#                                        extensions=extensions))

p_surrounds = re.compile(r'<p>(.*)</p>')

def unwrap_p(value):
  value = value.strip()
  match = p_surrounds.findall(value)
  if len(match) == 0:
    return value
  return match[0]

def slugify (value):
  return do_slugify(value)
