# How to install the website

## clone repository

First, clone the website

```
git clone git@gitlab.constantvzw.org:anais_berck/algoliterarypublishing.net.git
```

Enter the cloned repository
```
cd algoliterarypublishing.net
```

## create virtual environment with venv (optional)
```
python3 -m venv name_environment
```

activate the virtual environment
```
source name_environment/bin/activate
```


## Install the requirements

```
pip install -r requirements.txt
```

## start website locally
```
pelican -lr
```

or

```
make devserver
```

Open http://localhost:8000/ to check the state of the website

# Editing content

## Make a change

Create a markdown file or change an existing file and save it.

Open http://localhost:8000/ to check the new state of the website

## Push changes to git

Check which files you changed
```
git status  
```

Stage your changes

```
git add [name of file]
```

```
git commit -m " [comment that make it understandable what you just did (commit comments are poetry)]" 
```

Push them to the repository
```
git push
```

# website is saved in output folder, this is the folder that needs to be placed on the server
# copy using rsync
```
make rsync_upload
```

generates website and puts files in output folder, and transfers them to server using the right urls

## Writing content

The documentation of pelican has a section on how to add content to a general pelican install: https://docs.getpelican.com/en/stable/content.html#

It also lists which metadata properties can be set by default: https://docs.getpelican.com/en/stable/content.html#file-metadata

### Additional metadata

Some post types support additional metadata. The additional metadata listed below:

#### algoliterary-works
- title
- algorithm
- trees
- humans
- language
- published
- license
- repository
- publication_url
- support
- thanks

#### oracle-stories
- cards

#### activities
(tbd)