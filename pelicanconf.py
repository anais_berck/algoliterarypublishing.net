#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Anaïs Berck'
SITENAME = 'An Algoliterary Publishing House'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Brussels'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'theme'

DEFAULT_DATE = 'fs'

DIRECT_TEMPLATES = ['index']

FORMATTED_FIELDS = ['license', 'support', 'trees', 'summary']

SUMMARY_MAX_LENGTH = 150

import sys, os.path
sys.path.append(os.path.dirname(os.path.abspath(__file__)))

from algoliterary_filters import display_if_set, get_category_articles, make_markdown_filter, unwrap_p, slugify

JINJA_FILTERS = {
  'get_category_articles': get_category_articles,
  'display_if_set': display_if_set,
  'markdown_filter': make_markdown_filter(),
  'unwrap_p': unwrap_p,
  'slugify': slugify
}

IMAGE_PROCESS = {}