Title: Atelier sur les plantes
Date: 19-11-2021

# Une bibliothèque vivante de plantes, atelier avec les étudiants de St-Luc

[TOC]

## Concept

### Inspiration

De l'électro-culture aux récentes expériences de touché, les utilisations de l'électricité pour connecter et favoriser notre relation avec les plantes sont nombreuses.

![électro-culture : deux plantes, l'une avec électrode, l'autre sans. Illustration d'une expérience visant à prouver l'efficacité de la culture des plantes à l'électricité ](https://upload.wikimedia.org/wikipedia/commons/2/2d/EKult022.jpg)

[*électroculture : deux plantes, l'une avec électrode, l'autre sans. Illustration d'une expérience visant à prouver l'efficacité de la culture des plantes à l'électricité* ](https://fr.wikipedia.org/wiki/%C3%89lectroculture#/media/Fichier:EKult022.jpg)

Nous poursuivons ici cette tradition, comme une façon de donner du sens à la technologie dont nous disposons, et comme excuse pour passer du temps avec les plantes.

### Déroulement

L'atelier une bibliothèque vivante de plantes a pour but de relier les textes, les arbres et les humains grâce à l'utilisation du microcontrôleur ESP32.

L'idée est de créer un petit point d'accès wifi, lié à une plante, qui publiera différents textes en fonction de sa conductance et de l'humidité de son sol.

## set-up (30 minutes de souffrance, tenez bon)

### Éléments nécessaires

Pour participer à cet atelier, vous aurez besoin :

un microcontrôleur esp32 : pour cet atelier nous avons utilisé celui proposé par [AZdelivery](https://www.az-delivery.de/en/products/esp32-developmentboard)

un câble microUSB

un capteur d'humidité du sol avec des câbles Dupont

un câble crocodile/une pince crocodile

### Installation de l'IDE Arduino

Pour cet atelier, nous utiliserons l'IDE Arduino. Il s'agit d'un IDE (Integrated Development Environment - environnement de programmation intégré) open source et facile à utiliser pour télécharger du code vers un microcontrôleur. Il a été développé pour une gamme de microcontrôleurs également appelés arduino (c'est pourquoi il peut prêter à confusion). On parle bien ici du software et non du hardware

Pour l'installer, [vous pouvez suivre ce lien](https://www.arduino.cc/en/software) et cliquer sur le lien de téléchargement adapté à votre système d'exploitation (macOS, windows ou Linux)

### Installation du pilote USB de l'esp32

L'esp32 est un microcontrôleur, ce qui signifie qu'il peut effectuer des tâches informatiques simples comme lire un capteur, imprimer un message sur un canal de communication ou effectuer des opérations mathématiques.

L'ESP32 n'est pas une carte arduino traditionnelle et nécessite l'installation d'un pilote spécifique pour pouvoir communiquer avec votre ordinateur. Un driver/pilote est un petit programme qui va permettre la communication entre votre ordinateur et un autre périphérique, en d'autres termes, il va permettre à votre ordinateur de reconnaître l'ESP32.

Vous pouvez trouver le pilote [sur ce site] (https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers), sélectionnez le lien correspondant à votre système d'exploitation (macOS, Windows, Linux) et suivez les instructions.



### Installation de la bibliothèque pour esp32 sur l'IDE arduino 1/2

Maintenant votre ordinateur reconnaît l'esp32, mais l'IDE arduino doit savoir quelle langue lui parler. Pour que les deux communiquent de manière fluide, nous devons installer une bibliothèque pour l'esp32 sur l'IDE arduino.

1. Ouvrez l'IDE Arduino. Assurez-vous que vous êtes à la version 1.8 ou plus, si non, mettez à jour votre IDE avec la dernière version.

2. Cliquez sur le menu *File* dans la barre de menu supérieure.

3. Cliquez sur l'élément de menu *Préférences*. Cela ouvrira une boîte de dialogue de préférences.

   <img src="preference.png" alt="preference" style="zoom : 50% ;" />

4. Vous devriez être sur l'onglet *Paramètres* de la boîte de dialogue Préférences par défaut.

5. Recherchez la zone de texte intitulée "*Additional Boards Manager URLs*".

6. S'il y a déjà du texte dans cette zone, ajoutez une virgule à la fin de celui-ci, puis suivez l'étape suivante.

7. Collez le lien suivant dans la zone de texte - ***https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json***

8. Cliquez sur le bouton OK pour enregistrer le paramètre.



![preference_window](preference_window.png)

### Installation de la bibliothèque pour esp32 sur l'IDE arduino 2/2

Ensuite, vous devrez utiliser la nouvelle entrée pour ajouter les cartes ESPO32 à votre IDE Arduino. Pour ce faire, suivez cette procédure :

1. Dans l'IDE Arduino, cliquez sur le menu *Tools* dans la barre de menu supérieure.

2. Faites défiler vers le bas jusqu'à l'entrée *Board:* (c'est-à-dire *Board : Arduino/Genuino Uno*).

3. Un sous-menu s'ouvre lorsque vous mettez en évidence l'entrée *Board:*.

4. En haut du sous-menu se trouve *Boards Manager*. Cliquez dessus pour ouvrir la boîte de dialogue Boards Manager.

   ![gestionnaire de cartes](gestionnaire de cartes.png)



5. Dans la boîte de recherche du gestionnaire de cartes, entrez "*esp32*".

   ![gestionnaire de carte window](gestionnaire de carte window.png)

6. Vous devriez voir une entrée pour "*esp32 by Espressif Systems*". Mettez cette entrée en surbrillance et cliquez sur le bouton *Installer*.

7. Ceci va installer les cartes ESP32 dans votre IDE Arduino.

### Sélection de la bonne carte et du bon port

1 - Une fois que votre bibliothèque de cartes est installée, vous pouvez sélectionner la carte dans le gestionnaire de cartes. Sélectionnez soit esp32 Dev Board, soit Node32s.

![Capture d'écran pour montrer comment sélectionner la bonne carte](bonne_carte.png)

2 - Ensuite vous pouvez sélectionner sur quel port il va communiquer (le port USB)

![Capture d'écran pour montrer quel port utiliser](bon_port.png)

Maintenant vous êtes prêt à créer des scripts et à les télécharger sur votre microcontrôleur ! Bien joué.

## Utilisation d'un capteur tactile ESP32 avec un câble crocodile (30-45 minutes)

### Détection de l'électricité

Voici le code que vous devez mettre dans votre IDE arduino avant de l'envoyer à votre esp-32

```
int touch_sensor_value=0;
void setup()
{
Serial.begin(115200);
Serial.println("Touch sensor - ESP32 - electric feel");
}

void loop()
{
touch_sensor_value = touchRead(T0);
Serial.print("Nous pouvons sentir = ");
Serial.println( touch_sensor_value);
delay(1000);
}
```

Pour vous assurer que vous n'avez pas fait d'erreur, vous pouvez d'abord vérifier avec le petit bouton avec une marque cochée.

![Capture de l'IDE arduino avec la marque cochée entourée de rouge](arduino_check.png)



Si vous n'avez pas d'erreur, vous pouvez l'envoyer avec le bouton flèche.

![Capture d’écran de l'IDE d'arduino avec la marque flèche entouré de rouge](arduino_arrow.png)



### Brancher le capteur



Avant de brancher votre capteur, **débranchez votre carte esp-32.**





![Pince crocodile attachée au G0 d'un microcontrôleur esp32 sur une table avec des câbles en arrière-plan](IMG_1566.png)

-> comme vous pouvez le voir la pince crocodile **est connectée à G4 qui est la touche 0** **(T0)** sur ce schéma de brochage

![schéma d'un pinout esp-32](esp-32-pinout_bigger.png)



Une fois votre capteur branché, vous pouvez brancher l'esp-32 par usb.

Afin de savoir si votre capteur tactile et votre code fonctionnent, vous devez ouvrir votre moniteur série. C'est un outil qui vous permet de voir la conversation entre l'esp32 et votre ordinateur.

Pour ce faire, allez dans Tool>Serial Monitor.

Si tout va bien, vous devriez être capable de voir les valeurs de votre capteur tactile.

Maintenant si nous détaillons ce script :

**1- Sommer les acteurs de notre processus.**

Nous déclarons d'abord une variable, quelque chose qui sera surveillé, quelque chose qui existe pour l'esp32, dans notre cas la valeur du capteur, combien d'électricité. Au début du processus, elle est à zéro.

`int touch_sensor_value=0;``

**2- Mise en place de la scène.**

void setup est un moyen de lancer la commande de l'esp-32, il ne sera exécuté qu'une seule fois.
`void setup()`

`{`

Dans notre cas, nous voulons communiquer avec l'esp-32 sur le canal qui représente la fréquence de baud de 115200, donc dans le setup vocal nous allons ouvrir ce canal.

 ``Serial.begin(115200);``

Et pour s'assurer que c'est ouvert, on va imprimer, dire quelque chose dessus

`Serial.println("Touch sensor - ESP32 - electric feel");`

Et puis nous allons fermer ce premier script de configuration.

`}`

**3- Décrire des événements récurrents/commandes**

Le décor est planté, alors maintenant nous allons décrire ce que nous voulons que le contrôleur fasse continuellement. Dans notre cas, c'est de vérifier la valeur sur la pince crocodile et de l'imprimer dans notre canal de communication, le serial 115200.

La boucle est un moyen de lancer une commande récurrente qui sera appelée encore et encore.

```
void loop()
{
```

Dans cette commande en boucle, nous demandons à l'esp-32 de lire la valeur de son capteur relié à sa broche T0 et de la stocker dans la variable touch-sensor-value. Combien d'énergie électrique y a-t-il ?

`touch_sensor_value = touchRead(T0);`

Puis nous imprimons cette valeur dans le canal de communication série

```
Serial.print("On peut sentir = ") ;
Serial.println( touch_sensor_value) ;
```

Et puis nous laissons l'esp32 se reposer une seconde avant de recommencer depuis le début de la boucle.

```
delay(1000) ;
}
```

Afin de voir ce qui est imprimé, nous devons ouvrir le moniteur série dans la section Outil et sélectionner le débit en bauds 115200

![Capture d'écran pour montrer comment ouvrir le moniteur série](serial_baud_1.png)

![Capture d'écran du moniteur série](serial_baud_2.png)

### Tâche :

Trouvez votre propre moyen de demander la permission d'attacher la pince crocodile à la plante. N'attachez pas la pince directement car elle se ferme assez fortement, attachez un morceau de bobine que vous mettez autour du tronc.

*"« Une fois de plus, je demande gentiment la permission de cueillir.
J’exprime mon respect envers l’identité individuelle de la plante, mais j’évalue aussi le bien-être de sa population. Il me faut consécutivement utiliser les deux hémisphères de mon cerveau pour entendre et écouter la réponse. L’hémisphère gauche, analytique, déchiffre les signes empiriques pour juger si la population est assez nombreuse et en bonne santé pour supporter une récolte, si le poireau a poussé en quantité suffisante pour partager. L’hémisphère droit, intuitif, déchiffre d’autres signes : la générosité, une espèce d’irradiation qui me dit « cueille-le », ou, parfois, une résistance qui me fait aussitôt ranger ma truelle. Je ne peux l’expliquer, c’est une sorte de savoir intuitif qui, à mes yeux, est aussi éloquent qu’un panneau d’interdiction. Cette fois, lorsque j’enfonce ma truelle, j’extrais une belle grappe de bulbes blancs étincelants, dodus, glissants et odorants. J’entends « oui ». Aussitôt, je fais don en sortant du tabac d’un vieux sachet. Après, je creuse. »

Extrait de: Robin Wall Kimmerer. « Tresser les herbes sacrées. » ".

Si vous avez l'impression qu'il vous l'a donné, attachez la pince à la plante et vérifiez le comportement lorsque vous touchez la plante sur un tronc, sur une feuille, près du sol.


### Faire apparaître du texte à chaque fois que le capteur est touché

Nous pouvons maintenant voir la valeur sur la pince crocodile. Nous voulons maintenant utiliser cette information pour déclencher un comportement, chaque fois que la valeur descend sous un certain niveau, nous voulons afficher un texte :

```c
int touch_sensor_value=0 ;
void setup()
{
Serial.begin(115200) ;
Serial.println("Capteur tactile - ESP32 - sensation électrique") ;
}

void loop()
{
touch_sensor_value = touchRead(T0) ;

Serial.print("Il peut sentir = ") ;
Serial.println( touch_sensor_value) ;

si (touch_sensor_value<40){
Serial.println(" | ") ;  
Serial.println(" <>|<> ") ;  
Serial.println(" <>~|~<> ") ;
Serial.println("<>~<>|<>~<> ") ;
Serial.println("<>~<>|<>~<> ") ;
Serial.println(" <>~|~<> ") ;
Serial.println(" | ") ;
Serial.println(" | ") ;
Serial.println("Pour chacune d'entre nous en tant que femme, il y a un endroit sombre à l'intérieur, où se cache et grandit notre véritable esprit") ;
Serial.println("\"belle/et dure comme une châtaigne/stanchions contre (y)notre cauchemar de faiblesse/\"** et d'impuissance.") ;
Serial.println("Audre Lorde") ;
Serial.println(" | ") ;  
Serial.println(" <>|<> ") ;
Serial.println(" <>~|~<> ") ;
Serial.println("<>~<>|<>~<> ") ;
Serial.println("<>~<>|<>~<> ") ;
Serial.println(" <>~|~<> ") ;
Serial.println(" | ") ;
Serial.println(" | ") ;
delay(10000) ;
}
else {
Serial.println(" ~|~ ") ;
Serial.println(" ~ | ~ ") ;
Serial.println(" ~ | ~ ") ;
Serial.println(" ~|~ ") ;
Serial.println(" | ") ;
Serial.println(" | ") ;
}
delay(1000) ;
}
```

Ce que nous faisons d'abord est de récupérer la valeur du capteur :

```c
touch_sensor_value = touchRead(T0) ;

Serial.print("On peut sentir = ") ;
Serial.println( touch_sensor_value) ;
```

Ensuite, nous ajoutons une condition qui dit : si la valeur est inférieure à 10, alors nous imprimons ce message, si elle est supérieure, nous imprimons cet autre message.
Nous procédons comme suit.
Nous ouvrons d'abord la condition

`if (touch_sensor_value<40){`

Puis nous imprimons le message (l'arbre est juste esthétiquement plaisant)

```
Serial.println("Pour chacune d'entre nous en tant que femme, il y a un endroit sombre à l'intérieur, où se cache et grandit notre véritable esprit") ;
Serial.println("\"belle/et dure comme une châtaigne/stanchions contre (votre)cauchemar de faiblesse/\"** et d'impuissance.") ;
```

Puis nous fermons cette condition

`}`

Et nous ajoutons une autre déclaration pour tous les autres cas qui ne sont pas conformes à cette première condition

```
else {
Serial.println(" ~|~ ") ;
[....]
}
```

### Tâche

Créez votre propre texte et vos propres éléments d'illustration lorsque la plante est touchée. Codez votre propre comportement pour l'interaction.



## Pause (15 minutes)





## Créer un serveur web avec l'esp-32 (45 minutes)

Dans notre dernière tâche, nous allons mettre en place un serveur web qui tourne de manière autonome sur l'esp-32 et qui peut réagir au toucher ou à l'humidité du sol.

```c
#include <WiFi.h>
#include <WebServer.h>
// SSID et mot de passe
const char* ssid = "Plant_poem" ; // Entrez votre SSID ici
const char* password = "" ; //Entrez votre mot de passe ici

// Détails de l'adresse IP
IPAddress local_ip(192, 168, 1, 1) ;
IPAddress gateway(192, 168, 1, 1) ;
IPAddress subnet(255, 255, 255, 0) ;
WebServer server(80) ; // Objet du WebServer (port HTTP, 80 par défaut)

// Contenu HTML & CSS qui s'affiche sur le serveur web
String HTML = "<!DOCTYPE html>\
<html>\
<body>\
<h1> &#127811 ; Hello There &#127811;;</h1>\
</corps>\
</html>" ;

void setup() {
  Serial.begin(115200) ;

  // Créez le SoftAP
  WiFi.softAP(ssid, password) ;
  WiFi.softAPConfig(local_ip, gateway, subnet) ;


  Serial.print("Connexion à mon point d'accès : ") ;
  Serial.println(ssid) ;

  server.on("/", handle_root) ;

  server.begin() ;
  Serial.println("Serveur HTTP démarré") ;
  delay(100) ;
}

void loop() {
  server.handleClient() ;
}

// Gestion de l'url racine (/)
void handle_root() {
  server.send(200, "text/html", HTML) ;
}
```

### Description du processus

**0- Appel des bibliothèques**

```c
#include <WiFi.h>
#include <WebServer.h>
```

Cette première partie comprendra du code placé dans un autre fichier et qui nous permettra d'aller plus vite en utilisant des fonctions déjà écrites par d'autres personnes.

**1- Enumération/présentation des acteurs de notre processus.**

```c
// SSID & Mot de passe
const char* ssid = "Plant_poem" ; // Entrez votre SSID ici !à changer !
const char* password = "" ; //Entrez votre mot de passe ici !laissez vide !

// Détails de l'adresse IP
IPAddress local_ip(192, 168, 1, 1) ;
IPAddress gateway(192, 168, 1, 1) ;
IPAddress subnet(255, 255, 255, 0) ;
WebServer server(80) ; // Objet du WebServer (port HTTP, 80 par défaut)

// Contenu HTML & CSS qui s'affiche sur le serveur web
String HTML = "<!DOCTYPE html>\".
<html>\
<body>\

<h1> &#127811 ; Hello There &#127811;;</h1>\\N

</corps>\N
</html>" ;
```

Ici on commence par déclarer deux variables, qui vont être le nom du réseau de votre réseau et son mot de passe.

Le deuxième paragraphe concerne l'adresse de votre réseau, son adresse IP, à laquelle vous allez vous connecter pour voir votre site web.

Le troisième paragraphe est le contenu de votre site web, ici en html.

**2- Mise en place du décor.**

```c
void setup() {
  Serial.begin(115200) ;

  // Créez le SoftAP
  WiFi.softAP(ssid, password) ;
  WiFi.softAPConfig(local_ip, gateway, subnet) ;


  Serial.print("Connexion à mon point d'accès : ") ;
  Serial.println(ssid) ;

  server.on("/", handle_root) ;

  server.begin() ;
  Serial.println("Serveur HTTP démarré") ;
  delay(100) ;
}
```

Ici, nous allons d'abord ouvrir le canal de communication pour une vitesse de transmission de 115200.

Puis créer notre propre point d'accès Wi-Fi en utilisant la variable que nous avons déclarée à l'étape précédente et faire connaître l'utilisateur en imprimant une déclaration sur le canal de communication.

Ensuite, nous allons spécifier ce qui se passe sur l'adresse "/" de notre serveur. Ici, nous appelons une autre fonction appelée handle_root. Le comportement de cette fonction sera expliqué dans l'étape suivante.

Puis activer notre serveur et le faire savoir par le canal de communication.

**3- Décrire les événements récurrents/commandes**

```c
void loop() {
  server.handleClient() ;
}
```

Ici, nous demandons au contrôleur de gérer le client (votre navigateur) pour qu'il puisse accéder au serveur.

**4- fonction spéciale - handle root**

```c
// Gère l'url de la racine (/)
void handle_root() {
  server.send(200, "text/html", HTML) ;
}
```

Cette fonction détaille ce qui se passe lorsque quelqu'un se connecte à 192.168.1.1 sans aucun caractère après ceci (l'adresse racine)

## Comment ajouter un capteur tactile à ce code ? (15-45 minutes)

Afin d'ajouter un capteur au code, nous devons :

1- introduire son nom

2- faire en sorte que le script vérifie sa valeur de façon chronique

3- utiliser cette valeur toujours mise à jour dans une instruction if.

A quoi cela ressemblerait-il ? Pouvez-vous indiquer les étapes du code ci-dessous ?



```c
#include <WiFi.h>
#include <WebServer.h>
// SSID et mot de passe
const char* ssid = "Plant_poem" ; // Entrez votre SSID ici
const char* password = "" ; //Entrez votre mot de passe ici
int touch_sensor_value=0 ;

// Détails de l'adresse IP
IPAddress local_ip(192, 168, 1, 1) ;
IPAddress gateway(192, 168, 1, 1) ;
IPAddress subnet(255, 255, 255, 0) ;

WebServer server(80) ; // Objet du WebServer (port HTTP, 80 par défaut)

// Contenu HTML & CSS qui s'affiche sur le serveur web
String HTML = "<!DOCTYPE html>\
<html><head><meta http-equiv= \"Content-Type\" content=\"text/html;charset=utf-8\">\
<body>\
<h1> &#127811 </h1>\
<p>\"Nous tournons maintenant nos pensées vers les arbres. La Terre a beaucoup de familles d'arbres qui ont leurs propres fonctionnements et usages. Certains nous fournissent un abri et de l'ombre, d'autres des fruits, de la beauté et d'autres, des objets utiles. Beaucoup de personnes dans le monde utilisent un arbre comme symbole de paix et de force. D'un seul esprit, nous saluons et remercions la vie de l'arbre. Maintenant, nos esprits ne font qu'un. Extrait de: Robin Wall Kimmerer. Tresser les herbes sacrées.\"</p> \
<h1> &#127811 </h1>\
</body>\
</html>" ;

void setup() {
  Serial.begin(115200) ;

  // Créez le SoftAP
  WiFi.softAP(ssid, password) ;
  WiFi.softAPConfig(local_ip, gateway, subnet) ;


  Serial.print("Connexion à mon point d'accès : ") ;
  Serial.println(ssid) ;

  server.on("/", handle_root) ;

  server.begin() ;
  Serial.println("Serveur HTTP démarré") ;
  delay(100) ;
}

// Traitement de l'url racine (/)
void handle_root() {
  if (touch_sensor_value<40){
  server.send(200, "text/html", HTML) ;
  }
  else {
Serial.println("Rien à imprimer ici ~|~ ") ;
}
}

void loop() {
  server.handleClient() ;
  touch_sensor_value = touchRead(T0) ;
Serial.print("On peut sentir = ") ;
Serial.println( touch_sensor_value) ;
}

```

### Tâche

1- Créez votre propre librairie portative et accessible à distance grâce à ce code. Trouver un texte que vous avez envie de partager et faites le apparaitre.

### Option Facultative - Utilisation de l'esp-32 avec un capteur d'humidité

Pour utiliser l'esp-32 avec un capteur d'humidité, nous faisons un exercice similaire à celui avec le capteur tactile, avec l'ajout d'un circuit électrique pour le capteur d'humidité du sol, qui doit être électrifié lui-même, donc nous devons le connecter à une broche qui lui donne un courant de 5V et une broche qui lui donne une masse.

Une fois ceci fait, voici le code pour lire la sortie

```c
int SENSE= 2 ; // Entrée du capteur de sol sur la broche analogique A0
int value= 0 ;
void setup() {
Serial.begin(9600) ;
Serial.println("SOIL MOISTURE SENSOR") ;
Serial.println("-----------------------------");
}
void loop() {
value= analogRead(SENSE) ;
value= valeur/10 ;
Serial.println(valeur) ;
}
```

![un microcontrôleur esp32 avec des câbles dupont un jaune relié à la broche g2 et un gris gnd, en arrière plan il y a un capteur d'humidité](IMG_1564.png)

![un contrôleur esp-32 avec un câble orange relié à la broche V5, il y a une plaque avec une feuille en arrière-plan et un petit coin en aluminium d'un macbook pro](IMG_1565.png)

### Tâche

1- Comment pourriez vous intégrer ce capteur à votre serveur web?

### Liens techniques

[Documentation de l'Espressif sur l'esp-32](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/establish-serial-connection.html)

[Comment utiliser l'esp-32 comme capteur tactile](https://microcontrollerslab.com/esp32-touch-sensor-button-example/)

[Pilote pour l'esp-32](https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers)

[Site Arduino pour le téléchargement](https://www.arduino.cc/en/software)

[Communication wifi de l'esp-32 avec l'IDE arduino](https://microcontrollerslab.com/esp32-server-client-wi-fi-communication-arduino-ide/)

[création d'un serveur web avec l'esp-32](https://electropeak.com/learn/create-a-web-server-w-esp32/)

## Liens divers

[la vie secrète des plantes](https://www.youtube.com/watch?v=xvtcA46O-vA) - documentaire(-fiction) controversé - pour inspiration seulement

[wikipedia sur la vie secrète des plantes](https://en.wikipedia.org/wiki/The_Secret_Life_of_Plants)

[Une analyse de la chaine d'approvisionnement des minéraux nécessaires aux composant électroniques](https://www.dissentmagazine.org/online_articles/the-problem-with-conflict-minerals)

[Chaine d'approvisionnement de plusieurs matériaux d'un ordinateur](https://supplystudies.com/manifest/#manifest-https://supplystudies.com/manifest/json/samples/typical-laptop.json)

[Francis Hallé on specific trees](https://thereader.mitpress.mit.edu/walking-trees-parasitic-flowers-remarkable-plants-illustrated-guide)

[Tree thinking](https://placesjournal.org/article/tree-thinking/)

[Jagadish Chandra Bose](https://en.wikipedia.org/wiki/Jagadish_Chandra_Bose)

## Musique

[Mall Grab - Spirit Wave](https://open.spotify.com/track/5zfDn2VAMkLya9CRKEYZyb?si=c8a501261b734c9e)

[Soichi Terada - Bamboo fighter](https://open.spotify.com/track/0QKc8g97mQsbybkvHYN2Bi?si=f2956b81b57c4c58)

[Susobrino - La hoja de eucalipto](https://open.spotify.com/track/5jWvkNfoQW1A42btmzJinP?si=934bdf4d871d48df)

[Stevie Wonder - La vie secrète des plantes](https://open.spotify.com/album/3LSgLZrSXELqWt5eqb6XMY?si=y25zwRzbQlCJFWJiFTcAUA)
